<h1 style="font-size: 20px;">Edit Profile Picture</h1>
<hr>

<span style="display: flex; align-items: center;">
    <?php echo $this->Html->image($image, array('height' => '150px', 'width' => '150px', 'id' => 'preview', 'style' => 'margin: 10px;')); ?>
</span>
<?php
echo $this->Form->create('User', array('type' => 'file'));
echo $this->Form->file('User.pic');
echo $this->Form->end('Upload Profile Picture');
?>
<script type="text/javascript">
    $("#UserPic").on("input", function() {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (e.target.result.includes("image")) {
                document.getElementById("preview").src = e.target.result;
            } else {
                document.getElementById("preview").src = '/img/no_image.jpg';
            }
        };
        reader.readAsDataURL(document.getElementById("UserPic").files[0]);
    });
</script>