<h1 style="font-size: 20px;">Post Search Results</h1>
<hr>

<h1>Posts with "<?php echo h($terms) ?>" in their title or body</h1>
<?php
if (empty($posts)) {
    echo 'No posts found';
} else {
    foreach ($posts as $post) {
        $this->Putter->putPost($post, $this->Session->read('user.follows'));
    }
}
?>

<div style="text-align: center;">
    <?php
    echo $this->Paginator->numbers(array('first' => 2, 'last' => 2, 'modulus' => 2, 'ellipsis' => ' | ... | '));
    ?>
</div>