<h1 style="font-size: 20px;">User Search Results</h1>
<hr>

<h1>Users with "<?php echo h($terms) ?>" in their username</h1>
<?php
if (empty($users)) {
    echo 'No users found';
} else {
foreach ($users as $user) {
        $this->Putter->putUser($user);
    }
}
?>

<div style="text-align: center;">
    <?php
    echo $this->Paginator->numbers(array('first' => 2, 'last' => 2, 'modulus' => 2, 'ellipsis' => ' | ... | '));
    ?>
</div>