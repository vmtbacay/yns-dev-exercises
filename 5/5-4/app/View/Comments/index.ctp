<?php $this->Putter->putPost($post, $this->Session->read('user.follows')); ?>
<br>

<h1 style="font-size: 20px;">Comments</h1>
<hr>
<?php foreach ($comments as $comment): ?>
    <div style="border-style: ridge; margin: 10px; padding: 5px;">
        <div style="display: flex; align-items: center;">
            <?php
            echo '<img src="' . $this->webroot . 'img/' . $comment['User']['profile_pic'] . '" height="50px" width="50px">';
            $this->Space->spaceMaker();
            echo $this->Html->link($comment['User']['username'], array('controller' => 'posts', 'action' => 'index', $comment['User']['id'], null)) . '&nbsp;says:';
            ?>
        </div>
        <p style="margin: 10px"><?php echo h($comment['Comment']['body']); ?></p>
        <div style="margin: 10px; display: flex; justify-content: space-between;">
            <span style="text-align: left; color: gray">
                Posted on: <?php echo h($comment['Comment']['created']) ?>
            </span>
            <span>
                <?php
                if ($comment['Comment']['user_id'] === $this->Session->read('user.id')) {
                    ?>
                    <span class="deleteComment link" id="<?php echo $comment['Comment']['id'] ?>">
                        Delete
                    </span>
                    <?php
                    $this->Space->spaceMaker();

                    echo $this->Html->link(
                        'Edit', array('action' => 'edit', $comment['Comment']['id'])
                    );
                    $this->Space->spaceMaker();
                }
                ?>
            </span>
        </div>
    </div>
<?php endforeach; ?>

<div style="text-align: center;">
    <?php
    echo $this->Paginator->numbers(array('first' => 2, 'last' => 2, 'modulus' => 2, 'ellipsis' => ' | ... | '));
    ?>
</div>

<?php
echo $this->Form->create('Comment');
echo $this->Form->input('body', array('rows' => '3', 'label' => 'Add Comment'));
echo $this->Form->end('Submit Comment');
?>