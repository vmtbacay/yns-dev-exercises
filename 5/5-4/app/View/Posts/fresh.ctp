<h1 style="font-size: 20px;">The newest posts from the microverse</h1>
<hr>

<?php
if (empty($fresh)) {
    echo "No posts yet";
} else {
    foreach ($fresh as $post) {
        $this->Putter->putPost($post, $this->Session->read('user.follows'));
    }
}
?>