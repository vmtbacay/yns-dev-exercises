<h1 style="font-size: 20px;">Top 10 most liked posts in the last 24 hours</h1>
<hr>

<?php
if (empty($top)) {
    echo "No posts yet";
} else {
    foreach ($top as $post) {
        $this->Putter->putPost($post, $this->Session->read('user.follows'));
    }
}
?>