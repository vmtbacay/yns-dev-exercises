<?php
if ($this->Session->read('user.id') === $viewUser['id'] && $userOnly === null) {
    echo '<h1 style="font-size: 20px;">Add Post</h1>';
    echo '<hr>';
    echo $this->Form->create('Post', array('type' => 'file', 'url' => array('action' => 'add')));
    echo $this->Form->input('title');
    echo $this->Form->input('body', array('rows' => '3'));
    ?>
    <span style="margin-left: 10px;">Image:</span>
    <span style="display: flex; align-items: center;">
        <?php echo $this->Html->image('no_image.jpg', array('height' => '100px', 'id' => 'preview', 'style' => 'margin: 10px;')); ?>
        <button type="button" id="removeImage" hidden>Remove Image</button>
    </span>

    <?php
    echo $this->Form->file('Post.pic');
    echo $this->Form->end('Save Post');
} else {
    echo '<h1 style="font-size: 20px;">Search through user\'s posts</h1>';
    echo '<hr>';
    echo $this->Form->create(
        'Search', array('url' => array('controller' => 'searches', 'action' => 'posts', $viewUser['id']), 'type' => 'get')
    );
    echo $this->Form->input('terms', array('label' => '', 'type' => 'text', 'style' => 'width: 200px'));
    echo $this->Form->end('Search');
}

$allPosts = array_merge($posts, $reposts);
usort($allPosts, function($b, $a) {
    $x = !array_key_exists('Like', $a) ? 'Repost' : 'Post';
    $y = !array_key_exists('Like', $b) ? 'Repost' : 'Post';
     return strtotime($a[$x]['created']) - strtotime($b[$y]['created']);
});

define('PAGE_LIMIT', 5);
$postCount = count($allPosts);
$pages = max(ceil($postCount / PAGE_LIMIT), 1);
$page = min($pages, array_key_exists('page', $this->params['url']) ? $this->params['url']['page'] : 1);
$offset = ($page - 1)  * PAGE_LIMIT;
$end = min(($offset + PAGE_LIMIT), $postCount);
?>

<h1 style="font-size: 20px;"><?php echo $userOnly === null ? 'Feed' : 'User\'s Posts' ?></h1>
<hr><br>

<div id="posts">
    <?php
    if ($postCount === 0) {
        echo 'No posts yet';
    }
    for ($i = $offset; $i < $end; $i++) {
        $this->Putter->putPost($allPosts[$i], $this->Session->read('user.follows'));
    }
    
    if ($pages > 1) {
        ?>
        <div style="text-align: center;">
            <?php
            $pageNumbers = range(1, $pages);
            unset($pageNumbers[$page-1]);

            $left = false;
            if ($page > 1) {
                $left = true;
                unset($pageNumbers[$page-2]);
            }
            $right = false;
            if ($page < $pages) {
                $right = true;
                unset($pageNumbers[$page]);
            }

            $first = 2;
            $count = 0;
            foreach ($pageNumbers as $number) {
                if ($number > $page - 1) {
                    break;
                }

                if ($count >= $first) {
                    echo "... | ";
                    break;
                }

                echo $this->Html->link($number, $this->Html->url(array(
                    'controller' => 'posts',
                    'action' => 'index', $viewUser['id'], $userOnly,
                    "?" => array('page' => $number)
                )));
                echo  ' | ';
                unset($pageNumbers[$number-1]);
                $count++;
            }

            if ($left) {
                echo $this->Html->link($page - 1, $this->Html->url(array(
                    'controller' => 'posts',
                    'action' => 'index', $viewUser['id'], $userOnly,
                    "?" => array('page' => $page - 1)
                )));
                echo  ' | ';
            }

            echo '<span class="current">' . $page . '</span>';

            if ($right) {
                echo  ' | ';
                echo $this->Html->link($page + 1, $this->Html->url(array(
                    'controller' => 'posts',
                    'action' => 'index', $viewUser['id'], $userOnly,
                    "?" => array('page' => $page + 1)
                )));
            }

            $last = 2;
            if (count($pageNumbers) > $last) {
                echo " | ...";
            }
            foreach (array_slice($pageNumbers, -$last, $last)  as $number) {
                if ($number > $page) {
                    echo  ' | ';
                    echo $this->Html->link($number, $this->Html->url(array(
                        'controller' => 'posts',
                        'action' => 'index', $viewUser['id'], $userOnly,
                        "?" => array('page' => $number)
                    )));
                }
            }
            ?>
        </div>
        <?php
    }
    ?>
</div>
<script type="text/javascript">
    $("#PostPic").on("input", function() {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (e.target.result.includes("image")) {
                document.getElementById("preview").src = e.target.result;
                $("#removeImage").show();
            } else {
                document.getElementById("preview").src = '/img/no_image.jpg';
                $("#removeImage").hide();
            }
        };
        reader.readAsDataURL(document.getElementById("PostPic").files[0]);
    });

    $("#removeImage").click(function() {
        document.getElementById("preview").src = '/img/no_image.jpg';
        document.getElementById('PostPic').value = "";
        $(this).hide();
    });
</script>