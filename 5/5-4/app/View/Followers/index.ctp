<h1>Followers</h1>
<?php
if (empty($followers)) {
    echo 'No followers yet';
}

foreach ($followers as $follower) {
    $follower['User'] = $follower['FollowerProfile'];
    $this->Putter->putUser($follower);
}
?>

<div style="text-align: center;">
    <?php
    echo $this->Paginator->numbers(array('first' => 2, 'last' => 2, 'modulus' => 2, 'ellipsis' => ' | ... | '));
    ?>
</div>