<h1>Following</h1>
<?php
if (empty($follows)) {
    echo 'Following no one yet';
}

foreach ($follows as $follow) {
    $this->Putter->putUser($follow);
}
?>

<div style="text-align: center;">
    <?php
    echo $this->Paginator->numbers(array('first' => 2, 'last' => 2, 'modulus' => 2, 'ellipsis' => ' | ... | '));
    ?>
</div>