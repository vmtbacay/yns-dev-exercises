<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->fetch('title'); ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <script src="/js/jquery-3.4.1.js"></script>
    <?php
    echo $this->Html->meta('icon');
    echo $this->HTML->css('microblog.2');
    ?>
</head>
<body>
    <div class="topbar">
        <span class="home">
            <?php
            echo $this->Html->link(
                $this->Html->image(
                    'home.png', array('height' => '20px', 'width' => '20px'))
                    . '<span style="display: flex-inline; vertical-align: top;"> Home</span>',
                array('controller' => 'posts', 'action' => 'index'),
                array('style' => 'text-decoration: none;', 'title' => 'Sweet Home', 'escape' => false)
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('top.png', array('height' => '20px', 'width' => '20px'))
                . '<span style="display: flex-inline; vertical-align: top;"> Top Users',
                array('controller' => 'users', 'action' => 'top'),
                array('style' => 'text-decoration: none;', 'title' => 'Kings and queens of micro', 'escape' => false)
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('top.png', array('height' => '20px', 'width' => '20px'))
                . '<span style="display: flex-inline; vertical-align: top;"> Top Posts',
                array('controller' => 'posts', 'action' => 'top'),
                array('style' => 'text-decoration: none;', 'title' => 'Quality posts only', 'escape' => false)
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('fresh.png', array('height' => '20px', 'width' => '20px'))
                . '<span style="display: flex-inline; vertical-align: top;"> Fresh Users',
                array('controller' => 'users', 'action' => 'fresh'),
                array('style' => 'text-decoration: none;', 'title' => 'Micronewbies', 'escape' => false)
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('fresh.png', array('height' => '20px', 'width' => '20px'))
                . '<span style="display: flex-inline; vertical-align: top;"> Fresh Posts',
                array('controller' => 'posts', 'action' => 'fresh'),
                array('style' => 'text-decoration: none;', 'title' => 'The latest from the people', 'escape' => false)
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('random.png', array('height' => '20px', 'width' => '20px'))
                . '<span style="display: flex-inline; vertical-align: top;"> Random User!',
                array('controller' => 'users', 'action' => 'random'),
                array('style' => 'text-decoration: none;', 'title' => 'Jump into the unknown!', 'escape' => false)
            );
            echo '&nbsp;&nbsp;&nbsp;';

            echo $this->Html->link(
                $this->Html->image('random.png', array('height' => '20px', 'width' => '20px'))
                . '<span style="display: flex-inline; vertical-align: top;"> Random Post!',
                array('controller' => 'posts', 'action' => 'random'),
                array('style' => 'text-decoration: none;', 'title' => 'Jump into the unknown!', 'escape' => false)
            );
            ?>
        </span>
        <span style="float: right;">
            <table style="border-collapse: collapse; border-style: hidden;">
                <?php
                echo $this->Form->create(
                    'Search', array('url' => array('controller' => 'searches', 'action' => 'index'), 'type' => 'get')
                );
                ?>
                <tr>
                    <td><?php echo $this->Form->input('terms', array('label' => '', 'type' => 'text', 'style' => 'width: 200px')); ?></td>
                    <td><?php echo $this->Form->end('Search'); ?></td>
                    <td>
                        <?php
                        echo $this->Html->link(
                            'Log out', array('controller' => 'posts', 'action' => 'logout'), array('style' => 'text-decoration: none;')
                        );
                        ?>
                    </td>
                </tr>
            </table>
        </span>
    </div>
    <div class="sidebar">
        <?php
        $user = isset($viewUser) ? $viewUser : $this->Session->read('user');
        echo $this->Html->image($user['profile_pic'], array('height' => '150px', 'width' => '150px'));
        echo $this->Html->link(
            $user['username'], array(
                'controller' => 'posts', 'action' => 'index', $user['id'], null), array('style' => 'color: white; font-size: 15px'
            )
        );
        echo $this->Html->link('User\'s Posts', array('controller' => 'posts', 'action' => 'index', $user['id'], 1));
        echo $this->Html->link('Following', array('controller' => 'followers', 'action' => 'following', $user['id']));
        echo $this->Html->link('Followers', array('controller' => 'followers', 'action' => 'index', $user['id']));
        if ($this->Session->read('user.id') === $user['id']) {
            echo $this->Html->link('Edit Username', array('controller' => 'users', 'action' => 'editUsername'));
            echo $this->Html->link('Edit Password', array('controller' => 'users', 'action' => 'editPassword'));
            echo $this->Html->link('Edit Email', array('controller' => 'users', 'action' => 'editEmail'));
            echo $this->Html->link('Edit Profile Picture', array('controller' => 'users', 'action' => 'editProfilePic'));
        } else {
            if (in_array($user['id'], $this->Session->read('user.follows'))) {
                echo $this->Form->postlink('Unfollow User', array('controller' => 'followers', 'action' => 'unfollow', $user['id']));
            } else {
                echo $this->Form->postlink('Follow User', array('controller' => 'followers', 'action' => 'follow', $user['id']));
            }
        }
        ?>
    </div>

    <div id="content" style="margin-left: 190px;">
        <div id="deleteModal" class="modal">
            <div class="modal-content">
              <p>Are you sure you want to delete that?</p>
              <button class="deleteYes" style="width: 50px;">Yes</button>
              <button class="deleteNo" style="width: 50px;">No</button>
            </div>
        </div>

        <div id="likeModal" class="modal-noti">
            <div class="modal-notif">
                <span style="font-size: 12px; font-weight: bold;">Liked!</span>
            </div>
        </div>

        <div id="unlikeModal" class="modal-noti">
            <div class="modal-notif">
                <span style="font-size: 12px; font-weight: bold;">Unliked!</span>
            </div>
        </div>

        <div id="repostModal" class="modal-noti">
            <div class="modal-notif">
                <span style="font-size: 12px; font-weight: bold;">Reposted!</span>
            </div>
        </div>

        <div id="unrepostModal" class="modal-noti">
            <div class="modal-notif">
                <span style="font-size: 12px; font-weight: bold;">Unreposted!</span>
            </div>
        </div>

        <?php
        echo $this->Flash->render();
        echo $this->fetch('content');
        ?>
    </div>
</body>
<script type="text/javascript">
    function indexPageSetup() {
        document.getElementsByClassName('sidebar')[0].style.height = (document.body.scrollHeight - 100) + 'px';

        $(".unlike").click(function() {
        unlike($(this));
        });
        $(".like").click(function(){
            like($(this));
        });
        $(".unrepost").click(function() {
            unrepost($(this));
        });
        $(".repost").click(function(){
            repost($(this));
        });

        $(".delete").click(function() {
            $("#deleteModal").css("display", "block");
            $(".deleteYes").attr("id", $(this).attr("id"));
            $(".deleteYes").unbind('click');

            $(".deleteYes").click(async function() {
                $.post("<?php echo Router::url(array('controller' => 'posts', 'action' => 'delete')); ?>/" + $(this).attr("id"));
                $(".deleteYes").unbind('click');
                $(window).unbind('click');
                await new Promise(resolve => setTimeout(resolve, 3000));
                location.reload();
                //ssessionStorage.setItem("Message.flash", );
            })
        })

        $(".deleteComment").click(function() {
            $("#deleteModal").css("display", "block");
            $(".deleteYes").attr("id", $(this).attr("id"));
            $(".deleteYes").unbind('click');

            $(".deleteYes").click(function() {
                $.post("<?php echo Router::url(array('controller' => 'comments', 'action' => 'delete')); ?>/" + $(this).attr("id"));
                location.reload();
            })
        })

        $(window).click(function(event) {
            if (event.target == document.getElementById("deleteModal")
                || event.target == document.getElementsByClassName("deleteNo")[0]
            ) {
              $("#deleteModal").css("display", "none");
            }
        })

        $(".current").html($(".current").html() + " - You are here")

        $("form").submit(function (event) {
            $(":submit").attr("disabled", true);
        });
    }

    function unlike(me) {
        $.post("<?php echo Router::url(array('controller'=>'posts','action'=>'unlike')); ?>/" + me.attr("id"));
        $("#" + me.attr("id") + ".unlike").unbind('click');
        $("#" + me.attr("id") + ".unlike").click(function() {
            like($(this));
        });
        $("#" + me.attr("id") + ".unlike").html("Like");
        $("#" + me.attr("id") + ".unlike").attr('class', 'like link');
        $("[id=likeCount-" + me.attr("id") + "]").html(parseInt($("[id=likeCount-" + me.attr("id") + "]").html()) - 1);
        $("[id$='Modal']").css("display", "none");
        $("#unlikeModal").css("display", "block");
        $("#unlikeModal").delay(1000).fadeOut();
    }

    function like(me) {
        $.post("<?php echo Router::url(array('controller'=>'posts','action'=>'like')); ?>/" + me.attr("id"));
        $("#" + me.attr("id") + ".like").unbind('click');
        $("#" + me.attr("id") + ".like").click(function() {
            unlike($(this));
        });
        $("#" + me.attr("id") + ".like").html("Unlike");
        $("#" + me.attr("id") + ".like").attr('class', 'unlike link');
        $("[id=likeCount-" + me.attr("id") + "]").html(parseInt($("[id=likeCount-" + me.attr("id") + "]").html()) + 1);
        $("[id$='Modal']").css("display", "none");
        $("#likeModal").css("display", "block");
        $("#likeModal").delay(1000).fadeOut();
    }

    async function unrepost(me) {
        $.post("<?php echo Router::url(array('controller'=>'posts','action'=>'unrepost')); ?>/" + me.attr("id"));

        $("[id=repostCount-" + me.attr("id") + "]").html(parseInt($("[id=repostCount-" + me.attr("id") + "]").html()) - 1);
        $("[id$='Modal']").css("display", "none");
        $("#unrepostModal").css("display", "block");
        $("#unrepostModal").delay(1000).fadeOut();
        $(".link").unbind('click');

        await new Promise(resolve => setTimeout(resolve, 2000));

        $.get("<?php echo Router::url(array('controller'=>'posts','action'=>'index', $user['id'], 0, 'false')); ?>/", function(data, status){
            let postsIndex = data.indexOf("<div id=\"posts\">");
            let posts = data.substring(postsIndex + 16);
            $("#posts").html(posts);
            
            indexPageSetup();
        });

        $("#" + me.attr("id") + ".unrepost").attr('class', 'repost link');
        $("#" + me.attr("id") + ".repost").html("Repost");
    }

    async function repost(me) {
        $.post("<?php echo Router::url(array('controller'=>'posts','action'=>'repost')); ?>/" + me.attr("id"));

        $("[id=repostCount-" + me.attr("id") + "]").html(parseInt($("[id=repostCount-" + me.attr("id") + "]").html()) + 1);
        $("[id$='Modal']").css("display", "none");
        $("#repostModal").css("display", "block");
        $("#repostModal").delay(1000).fadeOut();
        $(".link").unbind('click');

        await new Promise(resolve => setTimeout(resolve, 2000));

        $.get("<?php echo Router::url(array('controller'=>'posts','action'=>'index', $user['id'], 0, 'false')); ?>/", function(data, status){
            let postsIndex = data.indexOf("<div id=\"posts\">");
            let posts = data.substring(postsIndex + 16);
            $("#posts").html(posts);
            
            indexPageSetup();
        });

        $("#" + me.attr("id") + ".repost").attr('class', 'unrepost link');
        $("#" + me.attr("id") + ".unrepost").html("Unrepost");
    }

    indexPageSetup();
</script>
</html>