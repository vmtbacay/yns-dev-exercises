<?php
class SearchesController extends AppController {
    public $components = array('Flash', 'Session', 'Paginator');

    public $uses = array('User', 'Post', 'Follower');

    const PAGE_LIMIT = 5;

    public function index() {
        $terms = $this->params['url']['terms'];
        if ($terms === '') {
            $this->Flash->error(__('Search bar empty.'));
            return $this->redirect($this->referer());
        }
        $this->set('terms', $terms);

        $this->set('posts', $this->Post->find('all', array(
            'conditions' => array(
                'Post.deleted' => 0,
                'OR' => array(
                    'Post.title LIKE' => '%' . $terms . '%',
                    'Post.body LIKE' => '%' . $terms . '%'
                )
            ),
            'order' => array('Post.created DESC'),
            'recursive' => 2
        )));

        $this->set('users', $this->User->find('all', array(
            'conditions' => array(
                'User.username LIKE' => '%' . $terms . '%',
                'User.deleted' => 0
            ),
            'order' => array('User.created DESC'),
            'recursive' => 2
        )));
    }

    public function users() {
        $terms = $this->params['url']['terms'];
        $this->set('terms', $terms);
        if ($terms === '') {
            $this->Flash->error(__('Search bar empty.'));
            return $this->redirect($this->referer());
        }

        $this->Paginator->settings = array(
            'conditions' => array('User.deleted' => 0, 'User.username LIKE' => '%' . $terms . '%'),
            'limit' => self::PAGE_LIMIT,
            'order' => array('User.created DESC'),
            'recursive' => 2
        );

        try {
            $this->set('users', $this->Paginator->paginate('User'));
        } catch (Exception $e) {
            $totalRecords = count(
                $this->User->find('all', array(
                    'conditions' => array(
                        'User.username LIKE' => '%' . $terms . '%',
                        'User.deleted' => 0
                    ),
                    'order' => array('Post.created DESC')
                ))
            );
            return $this->redirect(
                array_merge(
                    array('action' => 'users', '?' => array('terms' => $terms)),
                    array('page' => ceil($totalRecords / self::PAGE_LIMIT))
                )
            );
        }
    }

    public function posts($userId = null) {
        $terms = $this->params['url']['terms'];
        $this->set('terms', $terms);
        if ($terms === '') {
            $this->Flash->error(__('Search bar empty.'));
            return $this->redirect($this->referer());
        }

        $this->Paginator->settings = array(
            'conditions' => array(
                'Post.deleted' => 0,
                'OR' => array(
                    'Post.title LIKE' => '%' . $terms . '%',
                    'Post.body LIKE' => '%' . $terms . '%'
                )
            ),
            'order' => array('Post.created DESC'),
            'limit' => self::PAGE_LIMIT,
            'recursive' => 2
        );
        if ($userId !== null) {
            $this->Paginator->settings['conditions']['Post.user_id'] = $userId;
        }

        try {
            $this->set('posts', $this->Paginator->paginate('Post'));
        } catch (Exception $e) {
            $totalRecords = count(
                $this->Post->find('all', array(
                    'conditions' => array(
                        'Post.deleted' => 0,
                        'OR' => array(
                            'Post.title LIKE' => '%' . $terms . '%',
                            'Post.body LIKE' => '%' . $terms . '%'
                        )
                    ),
                    'order' => array('Post.created DESC'),
                    'recursive' => 2
                ))
            );
            if ($userId !== null) {
                $this->Paginator->settings['conditions']['Post.user_id'] = $userId;
            }
            return $this->redirect(
                array_merge(
                    array('action' => 'posts', '?' => array('terms' => $terms)),
                    array('page' => ceil($totalRecords / self::PAGE_LIMIT))
                )
            );
        }
    }
}