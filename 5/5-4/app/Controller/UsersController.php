<?php
App::uses('CakeEmail', 'Network/Email');
class EmailConfig {
    public static $gmail = array(
        'host' => 'ssl://smtp.gmail.com',
        'port' => 465,
        'username' => 'ynsmicroblog@gmail.com',
        'password' => 'microbloG0',
        'transport' => 'Smtp'
    );
}

class UsersController extends AppController {
    public $components = array('Flash', 'Session');

    public $uses = array('User', 'Follower', 'UnactivatedUser');

    public function beforeFilter() {
    }

    public function signUp() {
        $this->layout = 'suli';

        $this->User->validator()->add('repass', 'required', array(
            'rule' => array('equalToField', 'password'),
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Re-entered password is different')
        );

       if ($this->request->is('post')) {
            $this->UnactivatedUser->query('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
            date_default_timezone_set('Asia/Singapore');
            $this->UnactivatedUser->deleteAll(array('UnactivatedUser.activation_code_date <=' => date("Y-m-d H:i:s", strtotime('- 30 minutes'))), false);
            $this->UnactivatedUser->query('SET GLOBAL FOREIGN_KEY_CHECKS=1;');

            $this->request->data['UnactivatedUser'] = $this->request->data['User'];
            $activationCode = '';
            for ($i = 0; $i < 6; $i++) {
                $activationCode .= rand(0, 9);
            }
            $this->request->data['UnactivatedUser']['activation_code'] = $activationCode;
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->User->query('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
                $this->User->deleteAll(array('User.username' => $this->request->data['User']['username']), false);
                $this->User->query('SET GLOBAL FOREIGN_KEY_CHECKS=1;');

                $this->UnactivatedUser->create();
                if ($this->UnactivatedUser->save($this->request->data)) {
                    return $this->redirect(array('action' => 'activation', $this->UnactivatedUser->id, 0));
                }
            }
        }
    }

    public function activation($id, $resend) {
        $this->layout = 'suli';
        $this->set('id', $id);

        $user = $this->UnactivatedUser->findById($id);

        if (empty($user)) {
            throw new NotFoundException(__('Invalid URL'));
        }

        if ($this->request->is('post')) {
            $this->UnactivatedUser->validator()
                ->add('code', 'required', array(
                    'rule' => array('equalToField', 'activation_code'),
                    'required' => true,
                    'allowEmpty' => false,
                    'message' => 'Incorrect code')
                )
                ->add('activation_code_date', array(
                    'rule' => array('activationExpiration'),
                    'required' => true,
                    'allowEmpty' => false,
                    'message' => 'Code expired'
                )
            );

            $user['UnactivatedUser']['code'] = $this->request->data['UnactivatedUser']['code'];
            $user['UnactivatedUser']['activated'] = 1;
            if($this->UnactivatedUser->save($user)) {
                unset($user['UnactivatedUser']['id']);
                $user['User'] = $user['UnactivatedUser'];
                $this->User->create();
                if ($this->User->save($user)) {
                    $this->UnactivatedUser->query('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
                    $this->UnactivatedUser->deleteAll(array('UnactivatedUser.id' => $id), false);
                    $this->UnactivatedUser->query('SET GLOBAL FOREIGN_KEY_CHECKS=1;');
                    return $this->redirect(array('action' => 'activated'));
                } else {
                    return $this->Flash->error(__('Username or email already taken.'));
                }
            }
            $resend = 0;
        }

        if ($this->referer() === Router::url(array('action' => 'signUp'), true) || $resend === '1') {
            if ($resend === '1') {
                $activationCode = '';
                for ($i = 0; $i < 6; $i++) {
                    $activationCode .= rand(0, 9);
                }
                $user['UnactivatedUser']['activation_code'] = $activationCode;
                $user['UnactivatedUser']['activation_code_date'] = date("Y-m-d H:i:s");
                if ($this->UnactivatedUser->save($user)) {
                    $this->Flash->success(__('A new code has been sent.'));
                }
            }
            $codeMail = new CakeEmail(EmailConfig::$gmail);
            $codeMail->from(array('code@microblog.com' => 'microblog'));
            $codeMail->to($user['UnactivatedUser']['email']);
            $codeMail->subject('Activation code');
            $codeMail->send(
                'Your code is ' . $user['UnactivatedUser']['activation_code'] . '. Activate it through this url: ' . Router::url(array('action' => 'activation', $id, 0), true)
            );
        }
    }

    public function activated() {
        $this->layout = 'suli';
    }

    public function login() {
        $this->layout = 'suli';

        if ($this->request->is('post')) {
            $this->UnactivatedUser->query('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
            date_default_timezone_set('Asia/Singapore');
            $this->UnactivatedUser->deleteAll(array('UnactivatedUser.activation_code_date <=' => date("Y-m-d H:i:s", strtotime('- 30 minutes'))), false);
            $this->UnactivatedUser->query('SET GLOBAL FOREIGN_KEY_CHECKS=1;');

            $user = $this->UnactivatedUser->findByUsername($this->request->data['User']['username']);
            if (!empty($user)) {
                return $this->Flash->error(__('Account not activated yet.'));
            }

            $user = $this->User->findByUsernameAndDeleted($this->request->data['User']['username'], 0);
            if (!empty($user) && $user['User']['password'] === $this->request->data['User']['password']) {
                $this->Session->write('user', $user['User']);

                $follows = array($this->Session->read('user.id'));
                $followIds = $this->Follower->find('all', array(
                    'conditions' => array('follower_id' => $this->Session->read('user.id'), 'Follower.deleted' => 0),
                    'fields' => 'user_id'
                ));
                foreach ($followIds as $followId) {
                    array_push($follows, $followId['Follower']['user_id']);
                }
                $this->Session->write('user.follows', $follows);

                return $this->redirect(array('controller' => 'posts'));
            } else {
                $this->Flash->error(__('Invalid username/password.'));
            }
        }
    }

    public function top() {
        date_default_timezone_set('Asia/Singapore');
        $this->set('top', $this->User->find('all', array(
            'conditions' => array('User.deleted' => 0),
            'order' => array('(SELECT COUNT(*) FROM followers WHERE followers.user_id = User.id AND followers.deleted = 0) DESC'),
            'limit' => 10,
            'recursive' => 2
        )));
    }

    public function fresh() {
        $this->set('fresh', $this->User->find('all', array(
            'conditions' => array('User.deleted' => 0),
            'order' => array('User.created DESC'),
            'recursive' => 2,
            'limit' => 10
        )));
    }

    public function random() {
        $user = $this->User->find('first', array(
            'conditions' => array('User.deleted' => 0),
            'order' => array('RAND()'),
            'limit' => 1
        ));
        return $this->redirect(array('controller' => 'posts', 'action' => 'index', $user['User']['id']));
    }

    public function editUsername() {
        if ($this->Session->read('user.id') === null) {
            return $this->redirect(array('controller' => 'users', 'action' => 'signUp'));
        }

        $this->User->validator()->remove('password');
        $this->User->validator()->remove('email');

        if ($this->request->is('post')) {
            $this->User->id = $this->Session->read('user.id');
            $this->request->data['User']['username'] = trim($this->request->data['User']['username']);
            $this->request->data['User']['modified'] = date("Y-m-d H:i:s");
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Your username has been updated.'));
                $this->Session->write('user.username', $this->request->data['User']['username']);
                return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your username.'));
        }
    }

    public function editEmail() {
        if ($this->Session->read('user.id') === null) {
            return $this->redirect(array('controller' => 'users', 'action' => 'signUp'));
        }

        $this->User->validator()->remove('username');
        $this->User->validator()->remove('password');

        if ($this->request->is('post')) {
            $this->User->id = $this->Session->read('user.id');
            $this->request->data['User']['email'] = trim($this->request->data['User']['email']);
            $this->request->data['User']['modified'] = date("Y-m-d H:i:s");
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Your email has been updated.'));
                $this->Session->write('user.email', $this->request->data['User']['email']);
                return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your email.'));
        }
    }

    public function editPassword() {
        $this->User->validator()->remove('username');
        $this->User->validator()->remove('email');
        $this->User->validator()->add('repass', array(
            'differentPass' => array(
                'rule' => array('equalToField', 'password'),
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Re-entered password is different'
            )
        ));

        if ($this->request->is('post')) {
            $this->request->data['User']['password'] = trim($this->request->data['User']['password']);
            $this->request->data['User']['repass'] = trim($this->request->data['User']['repass']);
            if ($this->request->data['User']['oldpass'] !== $this->Session->read('user.password')) {
                $this->Flash->error(__('Old password is incorrect.'));
            } else {
                $this->User->id = $this->Session->read('user.id');
                $this->request->data['User']['modified'] = date("Y-m-d H:i:s");
                if ($this->User->save($this->request->data)) {
                    $this->Flash->success(__('Your password has been updated.'));
                    $this->Session->write('user.password', $this->request->data['User']['password']);
                    return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
                }
                $this->Flash->error(__('Unable to update your password.'));
            }
        }
    }

    public function editProfilePic() {
        if ($this->Session->read('user.id') === null) {
            return $this->redirect(array('controller' => 'users', 'action' => 'signUp'));
        }
        $this->set('image', $this->Session->read('user.profile_pic'));

        $this->User->validator()->remove('username');
        $this->User->validator()->remove('password');
        $this->User->validator()->remove('email');
        $this->User->validator()->add('profile_pic', 'required', array(
            'rule' => array('chkImageExtension'),
            'required' => true,
            'allowEmpty' => false,
            'message' => 'Please Upload Valid Image.'
        ));

        if ($this->request->is('post')) {
            if ($this->request->data['User']['pic']['name'] === '') {
                return $this->Flash->error(__('Please choose a different profile picture.'));
            }
            if ($this->request->data['User']['pic']['error']) {
                return $this->Flash->error(__('File is too big. Maximum is 2MB.'));
            }

            $this->User->id = $this->Session->read('user.id');
            $this->request->data['User']['modified'] = date("Y-m-d H:i:s");
            $img_name = explode('.', $this->request->data['User']['pic']['name']);
            $target_dir = dirname(APP) . '/app/webroot/img/';
            $target_file = $target_dir . $img_name[0] . '.' . $img_name[1];
            $i = 1;
            while (file_exists($target_file)) {
                $target_file = $target_dir . $img_name[0] . $i . '.' . $img_name[1];
                $i++;
            }
            $this->request->data['User']['profile_pic'] = basename($target_file);
            if ($this->User->save($this->request->data)) {
                move_uploaded_file($this->request->data['User']['pic']['tmp_name'], $target_file);
                $this->Flash->success(__('Your profile picture has been updated.'));
                $this->Session->write('user.profile_pic', basename($target_file));
                return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
            }
            $this->Flash->error(__('Please Upload Valid Image.'));
        }
    }
}