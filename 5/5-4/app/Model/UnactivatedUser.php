<?php
class UnactivatedUser extends AppModel {
    public function equalToField($check, $otherField) {
        $fname = '';
        foreach ($check as $key => $value) {
            $fname = $key;
            break;
        }
        return $this->data[$this->name][$fname] === $this->data[$this->name][$otherField];
    }

    public function activationExpiration($check) {
        return time() - strtotime($check['activation_code_date'])  <= 1800;
    }

    public function chkImageExtension($data) {
       $return = true; 

       if($data['profile_pic'] != ''){
            $fileData   = pathinfo($data['profile_pic']);
            $ext        = $fileData['extension'];
            $allowExtension = array('gif', 'jpeg', 'png', 'jpg');

            if(in_array($ext, $allowExtension)) {
                $return = true; 
            } else {
                $return = false;
            }   
        } else {
            $return = false; 
        }   

        return $return;
    } 
}