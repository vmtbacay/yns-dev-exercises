$("#pic").on("input", function() {
    var reader = new FileReader();
    reader.onload = function (e) {
        if (e.target.result.includes("image")) {
            document.getElementById("preview").src = e.target.result;
            $("#removeImage").show();
        } else {
            document.getElementById("preview").src = "/img/no_image.jpg";
            $("#removeImage").hide();
        }
    };
    if (document.getElementById("pic").files[0] == null) {
        document.getElementById("preview").src = "/img/no_image.jpg";
        $("#removeImage").hide();
    }
    reader.readAsDataURL(document.getElementById("pic").files[0]);
});

$("#removeImage").click(function() {
    document.getElementById("preview").src = "/img/no_image.jpg";
    document.getElementById('pic').value = "";
    $(this).hide();
});