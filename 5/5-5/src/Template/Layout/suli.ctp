<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= $this->fetch('title') ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <script src="/js/jquery-3.4.1.js"></script>
    <!-- Include external files and scripts here (See HTML helper for more info.) -->
    <?php
    echo $this->Html->meta('icon');
    echo $this->HTML->css('microblog_3');
    echo $this->HTML->script('suliFormDisable', ['defer' => true]);
    ?>
</head>
<body>
    <!-- If you'd like some sort of menu to
    show up on all of your views, include it here -->
    <div class="topbar">
        <div class="home">
            <?php
            echo $this->Html->link(
                'Sign Up', 
                ['controller' => 'users', 'action' => 'signUp'], 
                ['style' => 'text-decoration: none;']
            );
            $this->Space->spaceMaker();
            echo $this->Html->link(
                'Login', 
                '/users/login', 
                ['style' => 'text-decoration: none;']
            );
            $this->Space->spaceMaker();
            echo $this->Html->link(
                'Request Api key', 
                ['controller' => 'users', 'action' => 'requestApiKey'], 
                ['style' => 'text-decoration: none;']
            );
            ?>
        </div>
    </div>
    <div id="content">
        <!-- Here's where I want my views to be displayed -->
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>
</body>
</html>