<h1 style="font-size: 20px;">Followers</h1>
<hr>
<?php
$isEmpty = true;

foreach ($followers as $follower) {
    if (!$follower['follower_profile']['deleted']) {
        $this->Putter->putUser($follower['follower_profile']);
        $isEmpty = false;
    }
}

if ($isEmpty) {
    ?>
    <div style="text-align: center;">
        <?= $this->Html->image('noUsers.png', ['height' => '450px', 'style' => 'display: inline;']) ?>
    </div>
    <?php
}
?>

<div style="text-align: center;">
    <?= $this->Paginator->numbers(
        ['first' => 2, 'last' => 2, 'modulus' => 2, 'before' => '<span hidden>']
    ) ?>
</div>