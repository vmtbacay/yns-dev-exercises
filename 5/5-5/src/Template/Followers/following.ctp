<h1 style="font-size: 20px;">Following</h1>
<hr>
<?php
$isEmpty = true;

foreach ($follows as $follow) {
    if (!$follow['user']['deleted']) {
        $this->Putter->putUser($follow['user']);
        $isEmpty = false;
    }
}

if ($isEmpty) {
    ?>
    <div style="text-align: center;">
        <?= $this->Html->image('noUsers.png', ['height' => '450px', 'style' => 'display: inline;']) ?>
    </div>
    <?php
}
?>

<div style="text-align: center;">
    <?= $this->Paginator->numbers(
        ['first' => 2, 'last' => 2, 'modulus' => 2, 'before' => '<span hidden>']
    ) ?>
</div>