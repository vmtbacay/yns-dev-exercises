<h1 style="font-size: 20px;">Edit Comment</h1>
<hr>

<?php
echo $this->Form->create($comment);
echo $this->Form->control('body', ['rows' => '3']);
echo $this->Form->submit('Save Comment');
echo $this->Form->end();
?>