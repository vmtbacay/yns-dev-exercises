<div class="sulibox" style="padding: 10px;">
    You've successfully activated your account. You may now log in.<br><br>
    <?= $this->Html->link('Login', ['action' => 'login']) ?>
</div>