<h1 style="font-size: 20px;">Edit Username</h1>
<hr>

<?php
echo $this->Form->create($user, ['novalidate' => true]);
echo $this->Form->control('username', ['label' => 'New Username', 'value' => '', 'maxlength' => '']);
echo $this->Form->submit('Save Username');
echo $this->Form->end();
?>