<div class="sulibox">
    <div style="margin: 4px; text-align: center; font-size: 20px">
        microblog.<br>
        it's a blog. but micro.
    </div>
    <?php
    echo $this->Form->create($user, ['novalidate' => true]);
    echo $this->Form->control('username', ['maxlength' => '']) . '<br>';
    echo $this->Form->control('password');
    echo $this->Form->control('repass', ['label' => 'Re-Enter Password', 'type' => 'password']) . '<br>';
    echo $this->Form->control('email');
    echo $this->Form->submit('Sign up');
    echo $this->Form->end();
    ?>

    <div style="margin: 8px;">
    <?= $this->Html->link('Login', ['action' => 'login']) ?>
    </div>
</div>