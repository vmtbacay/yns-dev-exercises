<div class="sulibox" style="float: right; margin-right: 125px;">
    <div style="margin: 4px; text-align: center; font-size: 20px">
        microblog.<br>
        it's a blog. but micro.
    </div>
    <?php
    echo $this->Form->create('User');
    echo $this->Form->control('username');
    echo $this->Form->control('password');
    echo $this->Form->submit('Login');
    echo $this->Form->end();
    ?>

    <div style="margin: 8px;">
    <?= $this->Html->Link('Sign up', ['action' => 'signUp']) ?>
    </div>
</div>
<div style="float: right; margin-right: 150px; text-align: center;">
    <img src="/img/micro-1.png" style="display: inline;">
    <br><br>
    what is microblog? it's a blog but small. petite. <i>micro</i>.<br>
    have you've ever been on any social media site and thought:<br>
    <b>"tHiS PlAcE Is tOo bIg!"</b> or <b>"ThIs hAs tOo mAnY FeAtUrEs! D:"</b><br>
    <br>
    well, this is the site you've been looking for.<br>
    no fancy rigamaroles, or any bells and whistles,<br>
    even the logo was just taken from google images.<br>
    barebones as can be, there's no chance of being overwhelmed<br>
    <br>
    so what are you waiting for?<br>
    nothing, that's what.<br>
    sign up now.
</div>