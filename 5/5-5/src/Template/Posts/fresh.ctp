<h1 style="font-size: 20px;">The newest posts from the microverse</h1>
<hr>

<?php
if (empty($fresh)) {
    echo $this->Html->image('noPostsYet.png');
} else {
    foreach ($fresh as $post) {
        $this->Putter->putPost($post, $this->getRequest()->getSession()->read('Auth.User.follows'));
    }
}
?>