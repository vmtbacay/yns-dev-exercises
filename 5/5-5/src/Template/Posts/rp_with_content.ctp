<?= $this->Html->script('rpcImagePreview', ['defer' => true]) ?>
<h1 style="font-size: 20px;">Repost with content</h1>
<hr>

Original post:<br>
<?php
$this->Putter->putPost(
    $post, 
    $this->getRequest()
        ->getSession()
        ->read('Auth.User.follows')
);
echo $this->Form->create($newPost, ['type' => 'file', 'novalidate' => true]);
echo $this->Form->control('title');
echo $this->Form->control('body', ['rows' => '3']);
?>
<span style="margin-left: 10px;">Image</span>
<span style="display: flex; align-items: center;">
    <?= $this->Html->image('no_image.jpg', ['height' => '100px', 'id' => 'preview', 'style' => 'margin: 10px;']) ?>
    <button type="button" id="removeImage" hidden>Remove Image</button>
</span>
<?php
echo $this->Form->control('pic', ['type' => 'file', 'label' => '']);
echo $this->Form->submit('Save Post');
echo $this->Form->end();
?>