<?= $this->Html->script('editPostImagePreview', ['defer' => true]) ?>
<h1 style="font-size: 20px;">Edit Post</h1>
<hr>

<?php
if (isset($rpc)) {
    echo 'Original Post:';
    if (!$rpc['deleted']) {
        $this->Putter->putPost(
            $rpc, 
            $this->getRequest()
                ->getSession()
                ->read('Auth.User.follows')
        );
    } else {
        ?>
        <div style="border-style: solid; border-color: gray; margin: 10px; padding: 10px;">
            Post is not available
        </div>
        <?php
    }
}

echo $this->Form->create($post, ['type' => 'file', 'novalidate' => true]);
echo $this->Form->control('title');
echo $this->Form->control('body', ['rows' => '3']);
?>
<span style="margin-left: 10px;">Image</span>
<span style="display: flex; align-items: center;">
    <?php
        $hidden = '';
        if ($image === null) {
            $image = 'no_image.jpg';
            $hidden = 'hidden';
        }
        echo $this->Html->image($image, ['height' => '100px', 'id' => 'preview', 'style' => 'margin: 10px;']);
    ?>
    <button type="button" id="removeImage" <?= $hidden ?>>Remove Image</button>
</span>
<?php
echo $this->Form->control('pic', ['type' => 'file', 'label' => '']);
echo $this->Form->control('remimg', ['label' => '', 'hidden' => 'true', 'value' => 'false']);
echo $this->Form->submit('Save Post');
echo $this->Form->end();
?>