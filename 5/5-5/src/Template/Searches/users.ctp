<h1 style="font-size: 20px;">User Search Results</h1>
<hr>

<h1>Users with "<?= h($terms) ?>" in their username</h1>
<?php
if ($users->isEmpty()) {
    ?>
    <div style="text-align: center;">
        <?= $this->Html->image('noUsers.png', ['height' => '450px', 'style' => 'display: inline;']) ?>
    </div>
    <?php
} else {
foreach ($users as $user) {
        $this->Putter->putUser($user);
    }
}
?>

<div style="text-align: center;">
    <?= $this->Paginator->numbers(
        ['first' => 2, 'last' => 2, 'modulus' => 2, 'before' => '<span hidden>']
    ) ?>
</div>