<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Routing\Router;

class PutterHelper extends Helper {
    public $helpers = ['Html', 'Space', 'Form'];

    public function putUser($user) {
        ?>
        <div style="border-style: solid; margin: 10px; border-width: 2px; border-radius: 5px;">
            <div style="margin: 10px; display : flex; justify-content: space-between;">
                <span style="display: inline-flex; align-items: center;">
                    <?php
                    echo $this->Html->image(
                        $user['profile_pic'],
                        ['height' => '50', 'width' => '50', 'style' => 'border-radius: 50%; border: 2px ridge']
                    );
                    $this->Space->spaceMaker();
                    echo $this->Html->link(
                        $user['username'],
                        ['controller' => 'posts', 'action' => 'index', $user['id'], null],
                        ['style' => 'text-decoration: none;']
                    );
                    ?>
                </span>
                <span>
                    <?php
                    if ($user['id'] !== $this->getView()
                            ->getRequest()
                            ->getSession()
                            ->read('Auth.User.id')
                    ) {
                        ?>
                        <div class="grow">
                            <?php
                            if (
                                in_array(
                                    $user['id'],
                                    $this->getView()
                                        ->getRequest()
                                        ->getSession()
                                        ->read('Auth.User.follows')
                                )
                            ) {
                                echo $this->Html->image(
                                    'unfollow.png',
                                    ['height' => '17', 'width' => '17', 'style' => 'vertical-align: middle;']
                                );
                                echo ' ';
                                echo $this->Form->postLink(
                                    'Unfollow User',
                                    ['controller' => 'followers', 'action' => 'unfollow', $user['id']]
                                );
                            } else {
                                echo $this->Html->image(
                                    'follow.png',
                                    ['height' => '17', 'width' => '17', 'style' => 'vertical-align: middle;']
                                );
                                echo ' ';
                                echo $this->Form->postLink(
                                    'Follow User',
                                    ['controller' => 'followers', 'action' => 'follow', $user['id']]
                                );
                            }
                            ?>
                        </div>
                        <?php
                    }
                    $followerCount = count($user['followers']);
                    ?>
                    <div style="display: inline-block; font-size: 16px; overflow: hidden;">
                        <?= ' ' . $followerCount . ' follower(s)' ?>
                    </div>
                </span>
            </div>
        </div>
        <?php
    }

    public function putPost($ogPost, $follows, $isRpc = false) {
        $userid = $this->getView()
            ->getRequest()
            ->getSession()
            ->read('Auth.User.id');
        $borderColor = $isRpc ? 'gray' : 'block';
        if ($ogPost->getSource() === 'Reposts') {
            $isRepost = 'id="post'. $ogPost['Repost']['post_id'] . 'user' . $ogPost['Repost']['user_id'] . '"';
        } else {
            $isRepost = null;
        }
        ?>
        <div style="
            border-style: solid;
            margin: 10px;
            border-color: <?= $borderColor ?>;
            border-width: 2px;
            border-radius: 5px;" <?= $isRepost ?>
        >
            <div style="margin: 10px; display : flex; justify-content: space-between;">
                <span style="display: inline-flex; align-items: center;">
                    <?php
                    $post = $ogPost;

                    if ($post->getSource() === 'Reposts' && !$isRpc) {
                        $post = $post['post'];
                    }

                    if (file_exists(dirname(APP) . '/webroot/img/' . $post['user']['profile_pic'])
                        && $post['user']['profile_pic'] !== null
                    ) {
                        echo $this->Html->image(
                            $post['user']['profile_pic'],
                            ['height' => '50','width' => '50', 'style' => 'border-radius: 50%; border: 2px ridge']
                        );
                    } else {
                        echo $this->Html->image('default_profile_pic.jpg', ['height' => '50','width' => '50']);
                    }
                    $this->Space->spaceMaker();
                    echo $this->Html->link(
                        $post['user']['username'],
                        ['controller' => 'posts', 'action' => 'index', $post['user']['id'], null],
                        ['style' => 'text-decoration: none;']
                    );
                    echo '&nbsp;says:&nbsp;';
                    echo $this->Html->link(
                        $post['title'],
                        ['controller' => 'comments', 'action' => 'index', $post['id'], null],
                        ['style' => 'text-decoration: none;']
                    );
                    ?>
                </span>
                <span style="color: gray">
                    <?php
                    if ($isRepost) {
                        echo 'Reposted on: ' . h($ogPost['created']) . ' by ' . h($ogPost['user']['username']);
                    }
                    ?>
                </span>
            </div>
            <div style="margin: 10px">
                <?php
                echo h($post['body']) . '<br>';
                if (file_exists(dirname(APP) . '/webroot/img/' . $post['image'])
                    && $post['image'] !== null
                ) {
                    echo '<br>'. $this->Html->image($post['image'], ['style' => 'height: 100px;']);
                }
                if ($post['repost_id'] !== null) {
                    ?>
                    <div style="margin: 10px; padding: 10px;">
                        <hr>
                        <?php
                        if (!$post['r_p_c']['deleted']) {
                            if ($isRpc) {
                                ?>
                                <div style="
                                    border-style: solid;
                                    border-color: gray;
                                    margin: 10px;
                                    padding: 10px;
                                    border-width: 2px;
                                    border-radius: 5px;
                                ">
                                    Original Post:
                                    <br>
                                    <?= $this->Html->link(Router::url(
                                        ['controller'=>'comments','action'=>'index', $post['r_p_c']['id']],
                                        true
                                    )); ?>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div>
                                    <?php $this->putPost($post['r_p_c'], $follows, true); ?>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div style="
                                border-style: solid;
                                border-color: gray;
                                margin: 10px;
                                padding: 10px;
                                border-width: 2px;
                                border-radius: 5px;
                            ">
                                Post is not available
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div style="margin: 10px; display: flex; justify-content: space-between;">
                <span style="text-align: left; color: gray">
                    Posted on: <?= h($post['created']) ?>
                </span>
                <?php
                if (!$isRpc) {
                ?>
                    <span>
                        <?php
                        if ($userid === $post['user_id']) {
                            if (!$isRepost) {
                                ?>
                                <div class="grow">
                                    <?= $this->Html->image(
                                        'delete.jpg',
                                        ['height' => '20', 'width' => '20', 'style' => 'vertical-align: top;']
                                    ) ?>
                                    <span class="delete link" id="<?= $post['id'] ?>">
                                        Delete
                                    </span>
                                </div>
                                &nbsp;
                                <?php
                            }
                            ?>

                            <div class="grow">
                                <?php
                                echo $this->Html->image(
                                    'edit.png',
                                    ['height' => '15', 'width' => '15', 'style' => 'vertical-align: middle;']
                                );
                                echo ' ';
                                echo $this->Html->link(
                                    'Edit',
                                    ['controller' => 'posts', 'action' => 'edit', $post['id']],
                                    ['style' => 'text-decoration: none;']
                                );
                                ?>
                            </div>
                            &nbsp;
                            <?php
                        } else {
                            ?>
                            <div class="grow">
                                    <?php
                            if (in_array($post['user_id'], $follows)) {
                                echo $this->Html->image(
                                    'unfollow.png',
                                    ['height' => '17', 'width' => '17', 'style' => 'vertical-align: middle;']
                                );
                                echo ' ';
                                echo $this->Form->postLink(
                                    'Unfollow User',
                                    ['controller' => 'followers', 'action' => 'unfollow', $post['user']['id']]
                                );
                            } else {
                                echo $this->Html->image(
                                    'follow.png',
                                    ['height' => '17', 'width' => '17', 'style' => 'vertical-align: middle;']
                                );
                                echo ' ';
                                echo $this->Form->postLink(
                                    'Follow user',
                                    ['controller' => 'followers', 'action' => 'follow', $post['user']['id']]
                                );
                            }
                            ?>
                            </div>
                            &nbsp;
                            <?php
                        }

                        $hasLiked = false;
                        $likeId = 0;
                        foreach ($post['likes'] as $like) {
                            if ($like['user_id'] == $userid && !$like['deleted']) {
                                $hasLiked = true;
                                $likeId = $like['id'];
                                break;
                            }
                        }
                        $likeOrUnlike = $hasLiked ? ['unlike', 'Unlike'] : ['like', 'Like'];
                        ?>
                        <div class="grow">
                            <?= $this->Html->image(
                                'like.jpg',
                                ['height' => '20', 'width' => '20', 'style' => 'vertical-align: top;']
                            ) ?>
                            <span class="<?= $likeOrUnlike[0] ?> link" id="<?= $post['id'] ?>">
                                <?= $likeOrUnlike[1] ?>
                            </span>
                        </div>
                        <div style="display: inline-block; font-size: 16px; overflow: hidden;">
                            <span id="likeCount-<?= $post['id'] ?>"><?= count($post['likes']) ?></span> like(s)
                        </div>
                        &nbsp;

                        <?php
                        $hasReposted = false;
                        $repostId = 0;
                        foreach ($post['reposts'] as $repost) {
                            if ($repost['user_id'] == $userid && !$repost['deleted']) {
                                $hasReposted = true;
                                $repostId = $repost['id'];
                                break;
                            }
                        }
                        ?>
                        <div class="grow">
                            <?php
                            echo $this->Html->image(
                                'repost.png',
                                ['height' => '20', 'width' => '20', 'style' => 'vertical-align: middle;']
                            );
                            if ($hasReposted) {
                                ?>
                                <span class="unrepost link" id="<?= $post['id'] ?>">
                                    Delete repost
                                </span>
                                <?php
                            } else {
                                echo ' ' . $this->Html->link(
                                    'Repost ',
                                    ['controller' => 'posts', 'action' => 'repost', $post['id']],
                                    ['style' => 'text-decoration: none;']
                                );
                            }
                            echo '|&nbsp;' . $this->Html->link(
                                    'RP with content',
                                    ['controller' => 'posts', 'action' => 'rpWithContent', $post['id']],
                                    ['style' => 'text-decoration: none;']
                            );
                            ?>
                        </div>
                        <div style="display: inline-block; font-size: 16px; overflow: hidden;">
                            <span id="repostCount-<?= $post['id'] ?>">
                                <?= (count($post['reposts']) + count($post['r_p_c_posts'])) ?>
                            </span>
                            repost(s)
                        </div>
                        &nbsp;

                        <div class="grow">
                            <?php
                            echo $this->Html->image(
                                'comment.png', 
                                ['height' => '15', 'width' => '15', 'style' => 'vertical-align: middle;']
                            );
                            echo ' ';
                            echo $this->Html->link(
                                'Comment', 
                                ['controller' => 'comments', $post['id']], 
                                ['style' => 'text-decoration: none;']
                            );
                            ?>
                        </div>
                        <div style="display: inline-block; font-size: 16px; overflow: hidden;">
                            <?= ' ' . count($post['comments']) . ' comment(s)';?>
                        </div>
                    </span>
                <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
}