<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Core\Exception\Exception;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use RuntimeException;
use InvalidArgumentException;

class FollowersController extends AppController {
    public function initialize() {
        parent::initialize();

        $this->loadModel('Followers');
        $this->loadModel('Users');

        $this->loadComponent('Flash');
    }

    public function index($id = null) {
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
            }
        }

        if ($id === null) {
            $id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
        }
        $viewUser = $this->Users->findByIdAndDeleted($id, 0)
            ->first();
        if (empty($viewUser)) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid user', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            throw new Exception(__('Invalid user'));
        }
        $this->set('viewUser', $viewUser);

        $this->paginate = [
            'conditions' => ['Followers.user_id' => $id, 'Followers.deleted' => 0],
            'limit' => PAGE_LIMIT,
            'contain' => 'FollowerProfiles'
        ];
        $followers = $this->paginate($this->Followers);
        foreach ($followers as $follower) {
            $follower['follower_profile'] = $this->Users->findById($follower['follower_profile']['id'])
                ->contain('Followers')
                ->first();
        }

        try {
            $this->set('followers', $followers);

            if ($this->request->is('json')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact('status'));
                $this->set('_serialize', ['status', 'followers']);
                return;
            }
        } catch (Exception $e) {
            return $this->redirect(array_merge(
                ['action' => 'index'],
                ['page' => ceil(
                    $this->Follower->findAllByUserIdAndDeleted($id, 0)
                        ->count()
                    / PAGE_LIMIT
                )]
            ));
        }
    }

    public function following($id = null) {
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
            }
        }

        if ($id === null) {
            $id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
        }
        $viewUser = $this->Users->findByIdAndDeleted($id, 0)
            ->first();
        if (empty($viewUser)) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid user', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

             throw new Exception(__('Invalid user'));
        }
        $this->set('viewUser', $viewUser);

        $this->paginate = [
            'conditions' => ['Followers.follower_id' => $id, 'Followers.deleted' => 0],
            'limit' => PAGE_LIMIT,
            'contain' => 'Users'
        ];
        $follows = $this->paginate($this->Followers);
        foreach ($follows as $follow) {
            $follow['user'] = $this->Users->findById($follow['user']['id'])
                ->contain('Followers')
                ->first();
        }

        try {
            $this->set('follows', $follows);

            if ($this->request->is('json')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact('status'));
                $this->set('_serialize', ['status', 'follows']);
                return;
            }
        } catch (Exception $e) {
            return $this->redirect(array_merge(
                ['action' => 'index'],
                ['page' => ceil(
                    $this->Follower->findAllByFollowerIdAndDeleted($id, 0)
                        ->count()
                    / PAGE_LIMIT
                )]
            ));
        }
    }

    public function follow($id) {
        if ($this->request->is('post')) {
            if ($this->request->is('json')) {
                try {
                    if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
                } catch (RuntimeException | InvalidArgumentException $e) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if (!$token->verify(new Sha256(), 'microblog')) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
            }

            $user_id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
            if ($id == $user_id) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Unable to follow self', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                $this->Flash->error(__('Unable to follow self'));
                return $this->redirect($this->referer());
            }

            $user = $this->Users->findByIdAndDeleted($id, 0)
                ->first();
            if (!$user) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Invalid user', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

            throw new Exception(__('Invalid user'));
            }

            $follow = $this->Followers->findByUserIdAndFollowerId($id, $user_id)
                ->first();
            if ($follow) {
                if ($follow->deleted == 0) {
                    if ($this->request->is('json')) {
                        $status = ['message' => 'Already following that user', 'code' => '400'];
                        $this->set(compact('status'));
                        $this->set('_serialize', 'status');
                        return;
                    }

                    $this->Flash->error(__('Already following that user.'));
                    return $this->redirect($this->referer());
                }
                $follow->modified = date("Y-m-d H:i:s");
                $follow->deleted = 0;
            } else {
                $follow = $this->Followers->newEntity();
                $follow->user_id = $id;
                $follow->follower_id = $user_id;
            }
            if ($this->Followers->save($follow)) {
                if ($this->request->is('json')) {
                    $follow = $this->Followers->findById($follow->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'follow']));
                    $this->set('_serialize', ['status', 'follow']);
                    return;
                }

                $this->Flash->success(__('Successfully followed.'));
                $follows = $this->Auth->user('follows');
                array_push($follows, $id);
                $this->getRequest()
                    ->getSession()
                    ->write('Auth.User.follows', $follows);
                return $this->redirect($this->referer());
            } else {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Unable to follow that user', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                $this->Flash->error(__('Unable to follow that user.'));
            }
        }  else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
        return $this->redirect($this->referer());
    }

    public function unFollow($id) {
        if ($this->request->is('post')) {
            if ($this->request->is('json')) {
                try {
                    if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
                } catch (RuntimeException | InvalidArgumentException $e) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if (!$token->verify(new Sha256(), 'microblog')) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
            }

            $user_id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
            $follow = $this->Followers->findByUserIdAndFollowerIdAndDeleted($id, $user_id, 0)
                ->first();
            if (!$follow) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Already not following that user', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                $this->Flash->error(__('Already not following that user'));
                return $this->redirect($this->referer());
            }

            $follow->deleted = 1;
            $follow->deleted_date = date("Y-m-d H:i:s");
            if ($this->Followers->save($follow)) {
                if ($this->request->is('json')) {
                    $follow = $this->Followers->findById($follow->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'follow']));
                    $this->set('_serialize', ['status', 'follow']);
                    return;
                }

                $this->Flash->success(__('Successfully unfollowed.'));
                $follows = $this->Auth->user('follows');
                unset($follows[array_search($id, $follows)]);
                $this->getRequest()
                    ->getSession()
                    ->write('Auth.User.follows', $follows);
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Unable to unfollow.'));
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        } else {
            throw new Exception(__('Method not allowed'));
        }
        return $this->redirect($this->referer());
    }
}