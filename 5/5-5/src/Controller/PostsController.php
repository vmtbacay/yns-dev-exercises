<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Core\Exception\Exception;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use RuntimeException;
use InvalidArgumentException;

class PostsController extends AppController {
    public function initialize() {
        parent::initialize();

        $this->loadModel('Posts');
        $this->loadModel('Reposts');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
        $this->loadModel('Followers');
        $this->loadModel('Users');
    }

    public function index($id = null, $userOnly = null, $layout = 'default') {
        $this->viewBuilder()->setLayout($layout === 'false' ? false : $layout);
        $this->set('post', $this->Posts->newEntity());
        $this->set('userOnly', $userOnly);

        if ($id === null) {
            $id = $this->Auth->user('id');
            if ($this->request->is('json')) {
                try {
                    if (empty($this->request->getHeader('x-auth-token'))) {
                        $status = ['message' => 'No token', 'code' => '401'];
                        $this->set(compact('status'));
                        $this->set('_serialize', 'status');
                        return;
                    } else {
                        $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                    }
                } catch (RuntimeException | InvalidArgumentException $e) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
                $id = $token->getClaim('uid');
            }
        }
        $viewUser = $this->Users->findByIdAndDeleted($id, 0)
            ->first();
        if (!$viewUser) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid user', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            throw new Exception(__('Invalid user'));
        }
        $this->set('viewUser', $viewUser);

        $follows = [$id];
        $followIds = $this->Followers->find('all', [
            'conditions' => ['follower_id' => $id, 'deleted' => 0],
            'fields' => 'user_id'
        ])
            ->toArray();
        foreach ($followIds as $followId) {
            array_push($follows, $followId->user_id);
        }
        $follows = $userOnly === null ? $follows : [$id];

        $posts = $this->Posts->find('all', ['conditions' => ['Posts.user_id IN' => $follows, 'Posts.deleted' => 0]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $rpcIds = [-1];
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                array_push($rpcIds, $post['r_p_c']['id']);
            }
        }
        $postRpcs = $this->Posts->find('all', ['conditions' => ['Posts.id IN' => $rpcIds]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $postRpcs = Hash::combine($postRpcs, '{n}.id', '{n}');
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                $post['r_p_c'] = $postRpcs[$post['r_p_c']['id']];
            }
        }

        $reposts = $this->Reposts->find('all', [
                'conditions' => ['Reposts.user_id IN' => $follows, 'Reposts.deleted' => 0, 'Posts.deleted' => 0]
            ])
                ->contain(['Users', 'Posts' => ['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts']])
                ->toArray();
        $rpcIds = [-1];
        foreach ($reposts as $repost) {
            if ($repost['post']['r_p_c'] !== null) {
                array_push($rpcIds, $repost['post']['r_p_c']['id']);
            }
        }
        $repostRpcs = $this->Posts->find('all', ['conditions' => ['Posts.id IN' => $rpcIds]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $repostRpcs = Hash::combine($repostRpcs, '{n}.id', '{n}');
        foreach ($reposts as $repost) {
            if ($repost['post']['r_p_c'] !== null) {
                $repost['post']['r_p_c'] = $repostRpcs[$repost['post']['r_p_c']['id']];
            }
        }

        //cakePHP 3 Pagination is not used here because the 2 models used, Posts and Reposts,
        //are merged and sorted before being paginated.
        //cakePHP 3 Pagination is used almost everywhere else.
        $allPosts = array_merge($posts, $reposts);
        usort($allPosts, function($b, $a) {
            return strtotime($a['created']->format('Y-m-d H:i:s')) - strtotime($b['created']->format('Y-m-d H:i:s'));
        });
        $this->set('allPosts', $allPosts);

        if ($this->request->is('json')) {
            $status = ['message' => 'Invalid token', 'code' => '401'];

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact(['status', 'allPosts']));
                $this->set('_serialize', ['status', 'allPosts']);
                return;
            }
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
        }
    }

    public function top() {
        $posts = $this->Posts->find('all', [
            'conditions' => ['Posts.created >' => date("Y-m-d H:i:s", strtotime('- 1 day')),'Posts.deleted' => 0],
            'order' => ['(SELECT COUNT(*) FROM likes WHERE likes.post_id = Posts.id AND likes.deleted = 0) DESC'],
            'limit' => 10
        ])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $rpcIds = [-1];
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                array_push($rpcIds, $post['r_p_c']['id']);
            }
        }
        $postRpcs = $this->Posts->find('all', ['conditions' => ['Posts.id IN' => $rpcIds]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $postRpcs = Hash::combine($postRpcs, '{n}.id', '{n}');
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                $post['r_p_c'] = $postRpcs[$post['r_p_c']['id']];
            }
        }
        $this->set('top', $posts);

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact('status'));
                $this->set('_serialize', ['status', 'top']);
                return;
            } else {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }
    }

    public function fresh() {
        $posts = $this->Posts->find('all', [
            'conditions' => ['Posts.deleted' => 0],
            'order' => ['Posts.created DESC'],
            'limit' => 10
        ])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $rpcIds = [-1];
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                array_push($rpcIds, $post['r_p_c']['id']);
            }
        }
        $postRpcs = $this->Posts->find('all', ['conditions' => ['Posts.id IN' => $rpcIds]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $postRpcs = Hash::combine($postRpcs, '{n}.id', '{n}');
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                $post['r_p_c'] = $postRpcs[$post['r_p_c']['id']];
            }
        }
        $this->set('fresh', $posts);

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact('status'));
                $this->set('_serialize', ['status', 'fresh']);
                return;
            } else {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
            }
        }
    }

    public function random() {
        $post = $this->Posts->find('all', [
            'conditions' => ['Posts.deleted' => 0],
            'order' => ['RAND()'],
            'limit' => 1
        ])
            ->first();

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact(['status', 'post']));
                $this->set('_serialize', ['status', 'post']);
                return;
            } else {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
            }
        }

        return $this->redirect(['controller' => 'comments', 'action' => 'index', $post['id']]);
    }

    public function add() {
        if ($this->request->is('post')) {
            if ($this->request->is('json')) {
                try {
                    if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                    } else {
                        $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                    }
                } catch (RuntimeException | InvalidArgumentException $e) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if (!$token->verify(new Sha256(), 'microblog')) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if (empty($this->request->getdata()['title'])) {
                    $status = ['message' => 'Title cannot be empty', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
            }

            $request = $this->request->getData();
            $request['user_id'] = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');

            if (!empty($request['pic'])) {
                if (!$request['pic']['error']) {
                    $allowExtension = ['gif', 'jpeg', 'png', 'jpg'];
                    if(!in_array(explode('.', $request['pic']['name'])[1], $allowExtension)) {
                        if ($this->request->is('json')) {
                            $status = ['message' => 'Invalid image format', 'code' => '400'];
                            $this->set(compact('status'));
                            $this->set('_serialize', 'status');
                            return;
                        }

                        $this->Flash->error(__('Please upload a valid image'));
                        return $this->redirect(['action' => 'index']);
                    }

                    $img_name = explode('.', $request['pic']['name']);
                    $target_dir = dirname(APP) . '/webroot/img/';
                    $target_file = $target_dir . $img_name[0] . '.' . $img_name[1];
                    $i = 1;
                    while (file_exists($target_file)) {
                        $target_file = $target_dir . $img_name[0] . $i . '.' . $img_name[1];
                        $i++;
                    }
                    $request['image'] = basename($target_file);
                } else if (!empty($request['pic']['name'])) {
                    if ($this->request->is('json')) {
                        $status = ['message' => 'File is too big. Maximum is 2MB', 'code' => '400'];
                        $this->set(compact('status'));
                        $this->set('_serialize', 'status');
                        return;
                    }

                    $this->Flash->error(__('File is too big. Maximum is 2MB.'));
                    return $this->redirect(['action' => 'index']);
                }
            }

            $post = $this->Posts->newEntity($request);
            if (empty($post->title)) {
                $post->setError('title', ['Title cannot be empty']);
            }
            if (empty($post->body) && empty($post->image)) {
                $post->setError('body', ['Body and image cannot both be empty']);
            }

            if ($this->Posts->save($post)) {
                if (!empty($target_file)) {
                    move_uploaded_file($request['pic']['tmp_name'], $target_file);
                }

                if ($this->request->is('json')) {
                    $post = $this->Posts->findById($post->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'post']));
                    $this->set('_serialize', ['status', 'post']);
                    return;
                }

                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            if ($post->getErrors()) {
                if ($this->request->is('json')) {
                    $error_msg = [];
                    foreach ($post->getErrors() as $errors) {
                        array_push($error_msg, implode(', ', $errors));
                    }
                    $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                foreach ($post->getErrors() as $errors) {
                    $this->Flash->error(__(implode(', ', $errors)));
                }
            }
            return $this->redirect(['action' => 'index']);
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function edit($id) {
        if (!$id) {
            throw new Exception(__('Invalid post'));
        }

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (empty($this->request->getdata()['title'])) {
                $status = ['message' => 'Title cannot be empty', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }

        $post = $this->Posts->find('all', ['conditions' => ['Posts.id' => $id, 'Posts.deleted' => 0]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->first();
        if ($post['r_p_c'] !== null) {
            $post['r_p_c'] = $this->Posts->find('all', ['conditions' => ['Posts.id' => $post['r_p_c']['id']]])
                ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
                ->first();
            $this->set('rpc', $post['r_p_c']);
        }
        if (!$post) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        $user_id = $this->Auth->user('id');
        if ($this->request->is('json')) {
            $user_id = $token->getClaim('uid');
        }
        if ($post['user_id'] != $user_id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Unable to edit others\' posts.', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            $this->Flash->error(__('Unable to edit others\' posts.'));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }

        $this->set('post', $post);
        $this->set('image', $post['image']);
        $this->set('id', $id);

        if ($this->request->is(['post', 'put'])) {
            $request = $this->request->getData();
            $request['modified'] = date("Y-m-d H:i:s");

            if (!empty($request['pic']['name'])) {
                if (!$request['pic']['error']) {
                    $allowExtension = ['gif', 'jpeg', 'png', 'jpg'];
                    if(!in_array(explode('.', $request['pic']['name'])[1], $allowExtension)) {
                        if ($this->request->is('json')) {
                            $status = ['message' => 'Invalid image format', 'code' => '400'];
                            $this->set(compact('status'));
                            $this->set('_serialize', 'status');
                            return;
                        }
                        return $this->Flash->error(__('Please upload a valid image'));
                    }

                    $img_name = explode('.', $request['pic']['name']);
                    $target_dir = dirname(APP) . '/webroot/img/';
                    $target_file = $target_dir . $img_name[0] . '.' . $img_name[1];
                    $i = 1;
                    while (file_exists($target_file)) {
                        $target_file = $target_dir . $img_name[0] . $i . '.' . $img_name[1];
                        $i++;
                    }
                    $request['image'] = basename($target_file);
                } else if (!empty($request['pic']['name'])) {
                    if ($this->request->is('json')) {
                        $status = ['message' => 'File is too big. Maximum is 2MB.', 'code' => '400'];
                        $this->set(compact('status'));
                        $this->set('_serialize', 'status');
                        return;
                    }
                    return $this->Flash->error(__('File is too big. Maximum is 2MB.'));
                }
            }

            if (!empty($request['remimg']) && $request['remimg'] === 'true') {
                $request['image'] = null;
            }
            $post = $this->Posts->patchEntity($post, $request);
            if (empty($post->title)) {
                $post->setError('title', ['Title cannot be empty']);
            }
            if (empty($post->image) && empty($post->body)) {
                $post->setError('body', ['Body and image cannot both be empty']);
            }
            if ($this->Posts->save($post)) {
                if (
                    !empty($request['pic'])
                    && empty($request['pic']['error'])
                    && !empty($request['pic']['name'])
                ) {
                    move_uploaded_file($request['pic']['tmp_name'], $target_file);
                }

                if ($this->request->is('json')) {
                    $post = $this->Posts->findById($post->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'post']));
                    $this->set('_serialize', ['status', 'post']);
                    return;
                }
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(['controller' => 'comments', 'action' => 'index', $id]);
            } else if ($this->request->is('json')) {
                $error_msg = [];
                foreach ($post->getErrors() as $errors) {
                    array_push($error_msg, implode(', ', $errors));
                }
                $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function delete($id) {
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }

        if (!$id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        $post = $this->Posts->findByIdAndDeleted($id, 0)
            ->first();
        if (!$post) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        $user_id = $this->Auth->user('id');
        if ($this->request->is('json')) {
            $user_id = $token->getClaim('uid');
        }
        if ($post['user_id'] != $user_id) {
            return $this->Flash->error(__('Unable to delete others\' posts.'));
        }

        if ($this->request->is('post')) {
            $post->deleted = 1;
            $post->deleted_date = date("Y-m-d H:i:s");
            if ($this->Posts->save($post)) {
                if ($this->request->is('json')) {
                    $post = $this->Posts->findById($post->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'post']));
                    $this->set('_serialize', ['status', 'post']);
                    return;
                }

                return $this->Flash->success(__('Your post has been deleted.'));
            }

            if ($this->request->is('json')) {
                $status = ['message' => 'Unable to delete your post.', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            return $this->Flash->error(__('Unable to delete your post.'));
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        } else {
            throw new Exception(__('Method not allowed.'));
        }
    }

    public function repost($id) {
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }

        $post = $this->Posts->find('all', ['conditions' => ['Posts.id' => $id, 'Posts.deleted' => 0]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->first();
        if ($post['r_p_c'] !== null) {
            $post['r_p_c'] = $this->Posts->find('all', ['conditions' => ['Posts.id' => $post['r_p_c']['id']]])
                ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
                ->first();
            $this->set('rpc', $post['r_p_c']);
        }
        if (!$post) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }
        $this->set('post', $post);

        if ($this->request->is('post')) {
            $request = $this->request->getData();
            $user_id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
            $repost = $this->Reposts->findByUserIdAndPostIdAndDeleted($user_id, $id, 1)
                ->first();

            if ($repost) {
                $request['created'] = date("Y-m-d H:i:s");
                $request['deleted'] = 0;
                $repost = $this->Reposts->patchEntity($repost, $request);
            } else {
                $request['user_id'] = $user_id;
                $request['post_id'] = $id;
                $repost = $this->Reposts->newEntity($request);
            }

            $this->Reposts->save($repost);
            if ($this->request->is('json')) {
                $repost = $this->Reposts->findById($repost->id);
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact(['status', 'repost']));
                $this->set('_serialize', ['status', 'repost']);
                return;
            }
            $this->Flash->success(__('Your repost has been saved.'));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function rpWithContent($id) {
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (empty($this->request->getdata()['title'])) {
                $status = ['message' => 'Title cannot be empty', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }

        $post = $this->Posts->find('all', ['conditions' => ['Posts.id' => $id, 'Posts.deleted' => 0]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->first();
        if ($post['r_p_c'] !== null) {
            $post['r_p_c'] = $this->Posts->find('all', ['conditions' => ['Posts.id' => $post['r_p_c']['id']]])
                ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
                ->first();
            $this->set('rpc', $post['r_p_c']);
        }
        if (!$post) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        $this->set('id', $id);
        $this->set('post', $post);
        $this->set('newPost', $this->Posts->newEntity());

        if ($this->request->is('post')) {
            $request = $this->request->getData();
            $request['user_id'] = $this->Auth->user('id');
            if ($this->request->is('json')) {
                $request['user_id'] = $token->getClaim('uid');
            }
            $request['repost_id'] = $id;

            if (!empty($request['pic']['name'])) {
                if (!$request['pic']['error']) {
                    $allowExtension = ['gif', 'jpeg', 'png', 'jpg'];
                    if(!in_array(explode('.', $request['pic']['name'])[1], $allowExtension)) {
                        if ($this->request->is('json')) {
                            $status = ['message' => 'Invalid image format', 'code' => '400'];
                            $this->set(compact('status'));
                            $this->set('_serialize', 'status');
                            return;
                        }
                        return $this->Flash->error(__('Please upload a valid image'));
                    }

                    $img_name = explode('.', $request['pic']['name']);
                    $target_dir = dirname(APP) . '/webroot/img/';
                    $target_file = $target_dir . $img_name[0] . '.' . $img_name[1];
                    $i = 1;
                    while (file_exists($target_file)) {
                        $target_file = $target_dir . $img_name[0] . $i . '.' . $img_name[1];
                        $i++;
                    }
                    $request['image'] = basename($target_file);
                } else if (!empty($request['pic']['name'])) {
                    if ($this->request->is('json')) {
                        $status = ['message' => 'File is too big. Maximum is 2MB', 'code' => '400'];
                        $this->set(compact('status'));
                        $this->set('_serialize', 'status');
                        return;
                    }
                    return $this->Flash->error(__('File is too big. Maximum is 2MB'));
                }
            }

            $rpc = $this->Posts->newEntity($request);
            if (empty($rpc->title)) {
                $rpc->setError('title', ['Title cannot be empty']);
            }
            if (empty($rpc->body) && empty($rpc->image)) {
                $rpc->setError('body', ['Body and image cannot both be empty']);
            }
            if ($this->Posts->save($rpc)) {
                if (
                    !empty($request['pic'])
                    && empty($request['pic']['error'])
                    && !empty($request['pic']['name'])
                ) {
                    move_uploaded_file($this->request->getData()['pic']['tmp_name'], $target_file);
                }

                if ($this->request->is('json')) {
                    $rpc = $this->Posts->findById($rpc->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'rpc']));
                    $this->set('_serialize', ['status', 'rpc']);
                    return;
                }

                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            }
            if ($rpc->getErrors()) {
                if ($this->request->is('json')) {
                    $error_msg = [];
                    foreach ($rpc->getErrors() as $errors) {
                        array_push($error_msg, implode(', ', $errors));
                    }
                    $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                foreach ($rpc->getErrors() as $errors) {
                    $this->Flash->error(__(implode(', ', $errors)));
                }
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function unrepost($id) {
        if (!$this->request->is('json')) {
            $this->autoRender = false;
        }

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }

        if (!$id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid repost', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid repost'));
        }

        $user_id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
        $repost = $this->Reposts->findByUserIdAndPostIdAndDeleted($user_id, $id, 0)
            ->first();
        if (!$repost) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid repost', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid repost'));
        }

        if ($repost['user_id'] != $user_id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Unable to delete others\' reposts', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            return $this->Flash->error(__('Unable to delete others\' reposts'));
        }

        if ($this->request->is('post')) {
            $request = ['deleted' => 1, 'deleted_date' => date("Y-m-d H:i:s")];
            $repost = $this->Reposts->patchEntity($repost, $request);
            $this->Reposts->save($repost);

            if ($this->request->is('json')) {
                $repost = $this->Reposts->findById($repost->id);
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact(['status', 'repost']));
                $this->set('_serialize', ['status', 'repost']);
                return;
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function like($id) {
        if (!$this->request->is('json')) {
            $this->autoRender = false;
        }

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }

        if (!$id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        if ($this->request->is('post')) {
            $request = $this->request->getData();
            $user_id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
            $like = $this->Likes->findByUserIdAndPostIdAndDeleted($user_id, $id, 1)
                ->first();
            if ($like) {
                $request['modified'] = date("Y-m-d H:i:s");
                $request['deleted'] = 0;
                $like = $this->Likes->patchEntity($like, $request);
            } else {
                $request['user_id'] = $user_id;
                $request['post_id'] = $id;
                $like = $this->Likes->newEntity($request);
            }
            $this->Likes->save($like);

            if ($this->request->is('json')) {
                $like = $this->Likes->findById($like->id);
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact(['status', 'like']));
                $this->set('_serialize', ['status', 'like']);
                return;
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function unlike($id) {
        if (!$this->request->is('json')) {
            $this->autoRender = false;
        }

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }

        if (!$id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        $user_id = $this->request->is('json') ? $token->getClaim('uid') : $this->Auth->user('id');
        $like = $this->Likes->findByUserIdAndPostIdAndDeleted($user_id, $id, 0)
            ->first();
        if (!$like) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid post', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            throw new Exception(__('Invalid post'));
        }

        if ($like['user_id'] != $user_id) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Unable to delete others\' likes', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            return $this->Flash->error(__('Unable to delete others\' likes'));
        }

        if ($this->request->is('post')) {
            $request = ['deleted' => 1, 'deleted_date' => date("Y-m-d H:i:s")];
            $like = $this->Likes->patchEntity($like, $request);
            $this->Likes->save($like);

            if ($this->request->is('json')) {
                $like = $this->Likes->findById($like->id);
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact(['status', 'like']));
                $this->set('_serialize', ['status', 'like']);
                return;
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }
}