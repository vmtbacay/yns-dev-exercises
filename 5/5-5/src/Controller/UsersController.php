<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Core\Exception\Exception;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use RuntimeException;
use InvalidArgumentException;

class UsersController extends AppController {
    public function initialize() {
        parent::initialize();

        $this->loadModel('Followers');
        $this->loadModel('UnactivatedUsers');

        $this->loadComponent('Email');

        $this->Auth->allow(['index', 'signup', 'login', 'activation', 'activated', 'requestApiKey']);
    }

    public function index() {
        return $this->redirect(['action' => 'login']);
    }

    public function signup() {
        $this->viewBuilder()
            ->setLayout('suli');

        $this->set('user', $this->Users->newEntity());
        if ($this->request->is('post')) {
            $request = $this->request->getData();
            $this->UnactivatedUsers->query('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
            $this->UnactivatedUsers->deleteAll(
                ['UnactivatedUsers.activation_link_date <=' => date("Y-m-d H:i:s", strtotime('- 30 minutes'))],
                false
            );
            $this->UnactivatedUsers->query('SET GLOBAL FOREIGN_KEY_CHECKS=1;');

            $unactivatedUser = $this->UnactivatedUsers->newEntity($request);

            $user = $this->Users->newEntity($request);
            if ($this->Users->save($user)) {
                $this->Users->query('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
                $this->Users->deleteAll(['Users.username' => $request['username']], false);
                $this->Users->query('SET GLOBAL FOREIGN_KEY_CHECKS=1;');

                $unactivatedUser->password = password_hash($request['password'], PASSWORD_DEFAULT);
                $unactivatedUser->activation_link = sha1($unactivatedUser->username . $unactivatedUser->password);
                $unactivatedUser->activation_link_date = date("Y-m-d H:i:s");
                if ($this->UnactivatedUsers->save($unactivatedUser)) {
                    $this->Email->sendMail(
                        $unactivatedUser->email,
                        'Activation link',
                        'Your link is ' . Router::url(
                            ['action' => 'activated', $unactivatedUser->activation_link],
                            true
                        )
                    );

                    if ($this->request->is('json')) {
                        unset($unactivatedUser->repass);
                        $status = ['message' => 'User registered. Please activate account.', 'code' => '200'];
                        $this->set(compact(['status', 'unactivatedUser']));
                        $this->set('_serialize', ['status', 'unactivatedUser']);
                        return;
                    }

                    return $this->redirect(['action' => 'activation']);
                }
            } else if ($this->request->is('json')) {
                $error_msg = [];
                if (!empty($user->getErrors())) {
                    foreach ($user->getErrors() as $errors) {
                        array_push($error_msg, implode(', ', $errors));
                    }
                } else {
                    array_push($error_msg, 'Fields are empty');
                }
                $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            } else if ($user->getErrors()) {
                foreach ($user->getErrors() as $errors) {
                    $this->Flash->error(__(implode(', ', $errors)));
                }
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
        }
    }

    public function activation() {
        $this->viewBuilder()
            ->setLayout('suli');
    }

    public function activated($link) {
        $this->viewBuilder()
            ->setLayout('suli');

        $unactivatedUser = $this->UnactivatedUsers->findByActivationLink($link)
            ->first();
        if (empty($unactivatedUser)) {
            if ($this->request->is('json')) {
                $status = ['message' => 'Invalid link', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            throw new Exception(__('Invalid link'));
        }
        $user = $this->Users->newEntity();
        $user->username = $unactivatedUser->username;
        $user->password = $unactivatedUser->password;
        $user->email = $unactivatedUser->email;
        if ($this->Users->save($user, ['checkRules' => false])) {
            $this->UnactivatedUsers->query('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
            $this->UnactivatedUsers->deleteAll(['UnactivatedUsers.id' => $unactivatedUser['id']], false);
            $this->UnactivatedUsers->query('SET GLOBAL FOREIGN_KEY_CHECKS=1;');

            if ($this->request->is('json')) {
                $user = $this->Users->findById($user->id);
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact(['status', 'user']));
                $this->set('_serialize', ['status', 'user']);
                return;
            }
        } else {
            if ($this->request->is('json')) {
                $status = ['message' => 'Username or email already taken.', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            return $this->Flash->error(__('Username or email already taken.'));
        }
    }

    public function login() {
        $this->viewBuilder()
            ->setLayout('suli');

        if ($this->request->is('post')) {
            $this->UnactivatedUsers->query('SET GLOBAL FOREIGN_KEY_CHECKS=0;');
            $this->UnactivatedUsers->deleteAll(
                ['UnactivatedUsers.activation_link_date <=' => date("Y-m-d H:i:s", strtotime('- 30 minutes'))],
                false
            );
            $this->UnactivatedUsers->query('SET GLOBAL FOREIGN_KEY_CHECKS=1;');

            $user = $this->UnactivatedUsers->findByUsername($this->request->getData()['username'])
                ->toArray();
            if (!empty($user)) {
                return $this->Flash->error(__('Account not activated yet.'));
            }

            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);

                $follows = [$this->Auth->user('id')];
                $followIds = $this->Followers->find('all', [
                    'conditions' => ['follower_id' => $this->Auth->user('id'), 'deleted' => 0],
                    'fields' => 'user_id'
                ])
                    ->toArray();
                foreach ($followIds as $followId) {
                    array_push($follows, $followId->user_id);
                }
                $this->getRequest()->getSession()->write('Auth.User.follows', $follows);

                if ($this->request->is('json')) {
                    $token = (new Builder())->withClaim('uid', $this->Auth->user('id'))
                        ->getToken(new Sha256(), new Key('microblog'));

                    $status = ['message' => 'Logged In', 'code' => '200'];
                    $x_auth_token = (string) $token;
                    $data = ['user' => $user, 'x_auth_token' => $x_auth_token];
                    $this->set(compact(['status', 'data']));
                    $this->set('_serialize', ['status', 'data']);
                    return;
                }

                $this->redirect('/posts');
            } else {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Username or password is incorrect', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
                $this->Flash->error(__('Username or password is incorrect'));
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
        }
    }

    public function requestApiKey() {
        $this->viewBuilder()
            ->setLayout('suli');

        $key = (new Builder())->withClaim('rnd', sha1(time()))
                        ->getToken(new Sha256(), new Key('microblogKey'));
        $this->set('key', $key);
    }

    public function top() {
        $this->set('top', $this->Users->find('all', [
            'conditions' => ['Users.deleted' => 0],
            'order' => [
                '(SELECT COUNT(*) FROM Followers WHERE Followers.user_id = Users.id AND Followers.deleted = 0) DESC'
            ],
            'limit' => 10
        ])
            ->contain('Followers')
            ->toArray());

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact('status'));
                $this->set('_serialize', ['status', 'top']);
                return;
            } else {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }
    }

    public function fresh() {
        $this->set('fresh', $this->Users->find('all', [
            'conditions' => ['Users.deleted' => 0],
            'order' => ['Users.created DESC'],
            'limit' => 10
        ])
            ->contain('Followers')
            ->toArray());

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact('status'));
                $this->set('_serialize', ['status', 'fresh']);
                return;
            } else {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
            }
        }
    }

    public function random() {
        $user = $this->Users->find('all', [
            'conditions' => [
                'Users.deleted' => 0,
                '(SELECT COUNT(*) FROM Posts WHERE Posts.user_id = Users.id AND Posts.deleted = 0) >' => 0
            ],
            'order' => ['RAND()'],
            'limit' => 1
        ])
            ->first();

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact(['status', 'user']));
                $this->set('_serialize', ['status', 'user']);
                return;
            } else {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
            }
        }

        return $this->redirect(['controller' => 'posts', 'action' => 'index', $user['id']]);
    }

    public function editUsername() {
        $id = $this->Auth->user('id');
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            $id = $token->getClaim('uid');

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (empty($this->request->getdata()['username'])) {
                $status = ['message' => 'Username cannot be empty', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }
        $user = $this->Users->findById($id)
            ->first();
        $this->set('user', $user);

        if ($this->request->is('put')) {
            if ($user['id'] != $id) {
                return $this->Flash->error(__('Unable to edit others\' profiles.'));
            }

            $request = $this->request->getdata();
            $request['modified'] = date("Y-m-d H:i:s");
            $user = $this->Users->patchEntity($user, $request);
            if ($this->Users->save($user)) {
                if ($this->request->is('json')) {
                    $user = $this->Users->findById($user->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'user']));
                    $this->set('_serialize', ['status', 'user']);
                    return;
                }

                $this->Flash->success(__('Your username has been updated.'));
                $this->getRequest()
                    ->getSession()
                    ->write('Auth.User.username', $request['username']);

                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            } else if ($this->request->is('json')) {
                $error_msg = [];
                foreach ($user->getErrors() as $errors) {
                    array_push($error_msg, implode(', ', $errors));
                }

                $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function editPassword() {
        $id = $this->Auth->user('id');
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            $id = $token->getClaim('uid');

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (empty($this->request->getdata()['oldpass'])) {
                $status = ['message' => 'Old password cannot be empty', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (empty($this->request->getdata()['password'])) {
                $status = ['message' => 'New password cannot be empty', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (empty($this->request->getdata()['repass'])) {
                $status = ['message' => 'Re-entered new password cannot be empty', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }
        $user = $this->Users->findById($id)
            ->first();
        $this->set('user', $user);

        if ($this->request->is('put')) {
            if ($user['id'] != $id) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Invalid user id', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
                return $this->Flash->error(__('Unable to edit others\' profiles.'));
            }

            $request = $this->request->getData();
            if (!password_verify($request['oldpass'], $user->password)) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Old password is incorrect.', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
                return $this->Flash->error(__('Old password is incorrect.'));
            } else {
                $request['modified'] = date("Y-m-d H:i:s");
                $user = $this->Users->patchEntity($user, $request);
                if ($this->Users->save($user)) {
                    $user->password = password_hash($user->password, PASSWORD_DEFAULT);
                    $this->Users->save($user, ['checkRules' => false]);

                    if ($this->request->is('json')) {
                        $user = $this->Users->findById($user->id);
                        $status = ['message' => 'Request successful', 'code' => '200'];
                        $this->set(compact(['status', 'user']));
                        $this->set('_serialize', ['status', 'user']);
                        return;
                    }

                    $this->Flash->success(__('Your password has been updated.'));
                    return $this->redirect(['controller' => 'posts', 'action' => 'index']);
                } else if ($this->request->is('json')) {
                    $error_msg = [];
                    foreach ($user->getErrors() as $errors) {
                        array_push($error_msg, implode(', ', $errors));
                    }

                    $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function editEmail() {
        $id = $this->Auth->user('id');
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            $id = $token->getClaim('uid');

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if (empty($this->request->getdata()['email'])) {
                $status = ['message' => 'Email cannot be empty', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }
        $user = $this->Users->findById($id)
            ->first();
        $this->set('user', $user);

        if ($this->request->is('put')) {
            if ($user['id'] != $id) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Invalid user id', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                return $this->Flash->error(__('Unable to edit others\' profiles.'));
            }

            $request = $this->request->getdata();
            $request['email'] = trim($request['email']);
            $request['modified'] = date("Y-m-d H:i:s");
            $user = $this->Users->patchEntity($user, $request);
            if ($this->Users->save($user)) {
                if ($this->request->is('json')) {
                    $user = $this->Users->findById($user->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'user']));
                    $this->set('_serialize', ['status', 'user']);
                    return;
                }

                $this->Flash->success(__('Your email has been updated.'));
                $this->getRequest()
                    ->getSession()
                    ->write('Auth.User.email', $request['email']);
                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            } else if ($this->request->is('json')) {
                $error_msg = [];
                foreach ($user->getErrors() as $errors) {
                    array_push($error_msg, implode(', ', $errors));
                }

                $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }

    public function editProfilePic() {
        $id = $this->Auth->user('id');
        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            $id = $token->getClaim('uid');

            if (!$token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }
        $user = $this->Users->findById($id)
            ->first();
        $this->set('user', $user);
        $this->set('image', $this->Auth->user('profile_pic'));

        if ($this->request->is(['put', 'post'])) {
            if ($user['id'] != $id) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Invalid user id', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                return $this->Flash->error(__('Unable to edit others\' profiles.'));
            }

            $request = $this->request->getData();
            if (empty($this->request->getdata()['pic']) || empty($this->request->getdata()['pic']['name'])) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Profile pic cannot be empty', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                return $this->Flash->error(__('Please choose a picture'));
            }

            if ($request['pic']['error']) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'File is too big. Maximum is 2MB', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                return $this->Flash->error(__('File is too big. Maximum is 2MB.'));
            }

            $allowExtension = ['gif', 'jpeg', 'png', 'jpg'];
            if(!in_array(explode('.', $request['pic']['name'])[1], $allowExtension)) {
                if ($this->request->is('json')) {
                    $status = ['message' => 'Invalid image format', 'code' => '400'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                return $this->Flash->error(__('Please upload a valid image'));
            }

            $request['modified'] = date("Y-m-d H:i:s");
            $img_name = explode('.', $request['pic']['name']);
            $target_dir = dirname(APP) . '/webroot/img/';
            $target_file = $target_dir . $img_name[0] . '.' . $img_name[1];
            $i = 1;
            while (file_exists($target_file)) {
                $target_file = $target_dir . $img_name[0] . $i . '.' . $img_name[1];
                $i++;
            }
            $request['profile_pic'] = basename($target_file);

            $user = $this->Users->patchEntity($user, $request);
            if ($this->Users->save($user)) {
                move_uploaded_file($request['pic']['tmp_name'], $target_file);

                if ($this->request->is('json')) {
                    $user = $this->Users->findById($user->id);
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact(['status', 'user']));
                    $this->set('_serialize', ['status', 'user']);
                    return;
                }

                $this->Flash->success(__('Your profile picture has been updated.'));
                $this->getRequest()
                    ->getSession()
                    ->write('Auth.User.profile_pic', basename($target_file));
                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            } else if ($this->request->is('json')) {
                $error_msg = [];
                foreach ($user->getErrors() as $errors) {
                    array_push($error_msg, implode(', ', $errors));
                }

                $status = ['message' => implode(', ', $error_msg), 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
            return $this->Flash->error(__('Please Upload Valid Image.'));
        } else if ($this->request->is('json')) {
            $status = ['message' => 'Invalid Method', 'code' => '405'];
            $this->set(compact('status'));
            $this->set('_serialize', 'status');
            return;
        }
    }
}