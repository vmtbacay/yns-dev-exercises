<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Core\Exception\Exception;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use RuntimeException;
use InvalidArgumentException;

class SearchesController extends AppController {
    public function initialize() {
        parent::initialize();

        $this->loadModel('Posts');
        $this->loadModel('Followers');
        $this->loadModel('Users');

        $this->loadComponent('Flash');
    }

    public function index() {
        $terms = $this->request->getQuery('terms');
        if (empty($terms)) {
            if ($this->request->is('json')) {
                $status = ['message' => 'No search terms', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            $this->Flash->error(__('Search bar empty.'));
            return $this->redirect($this->referer());
        }
        $this->set('terms', $terms);

        $posts = $this->Posts->find('all', [
            'conditions' => [
                'Posts.deleted' => 0,
                'OR' => ['Posts.title LIKE' => '%' . $terms . '%', 'Posts.body LIKE' => '%' . $terms . '%']
            ],
            'order' => ['Posts.created DESC']
        ])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $rpcIds = [-1];
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                array_push($rpcIds, $post['r_p_c']['id']);
            }
        }
        $postRpcs = $this->Posts->find('all', ['conditions' => ['Posts.id IN' => $rpcIds]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $postRpcs = Hash::combine($postRpcs, '{n}.id', '{n}');
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                $post['r_p_c'] = $postRpcs[$post['r_p_c']['id']];
            }
        }
        $this->set('posts', $posts);

        $this->set('users', $this->Users->find('all', [
            'conditions' => ['Users.username LIKE' => '%' . $terms . '%', 'Users.deleted' => 0]
        ])
            ->contain('Followers')
            ->toArray());

        if ($this->request->is('json')) {
            try {
                if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
            } catch (RuntimeException | InvalidArgumentException $e) {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            if ($token->verify(new Sha256(), 'microblog')) {
                $status = ['message' => 'Request successful', 'code' => '200'];
                $this->set(compact('status'));
                $this->set('_serialize', ['status', 'posts', 'users']);
                return;
            } else {
                $status = ['message' => 'Invalid token', 'code' => '401'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }
        }
    }

    public function users() {
        $terms = $this->request->getQuery('terms');
        $this->set('terms', $terms);
        if (empty($terms)) {
            if ($this->request->is('json')) {
                $status = ['message' => 'No search terms', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            $this->Flash->error(__('Search bar empty.'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'conditions' => ['Users.deleted' => 0, 'Users.username LIKE' => '%' . $terms . '%'],
            'limit' => PAGE_LIMIT,
            'order' => ['Users.created DESC'],
            'contain' => ['Followers']
        ];

        try {
            $this->set('users', $this->paginate($this->Users));

            if ($this->request->is('json')) {
                try {
                    if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
                } catch (RuntimeException | InvalidArgumentException $e) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if ($token->verify(new Sha256(), 'microblog')) {
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact('status'));
                    $this->set('_serialize', ['status', 'users']);
                    return;
                } else {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
            }
        } catch (Exception $e) {
            $totalRecords = $this->Users->find('all', [
                'conditions' => ['Users.username LIKE' => '%' . $terms . '%', 'Users.deleted' => 0]
            ])
                ->count();
            return $this->redirect(
                array_merge(
                    ['action' => 'users', '?' => ['terms' => $terms]],
                    ['page' => ceil($totalRecords / PAGE_LIMIT)]
                )
            );
        }
    }

    public function posts($userId = null) {
        $terms = $this->request->getQuery('terms');
        $this->set('terms', $terms);
        if (empty($terms)) {
            if ($this->request->is('json')) {
                $status = ['message' => 'No search terms', 'code' => '400'];
                $this->set(compact('status'));
                $this->set('_serialize', 'status');
                return;
            }

            $this->Flash->error(__('Search bar empty.'));
            return $this->redirect($this->referer());
        }

        $conditions = [
            'Posts.deleted' => 0,
            'OR' => ['Posts.title LIKE' => '%' . $terms . '%', 'Posts.body LIKE' => '%' . $terms . '%']
        ];
        if ($userId !== null) {
            $conditions['Posts.user_id'] = $userId;
        }

        $this->paginate = [
            'conditions' => $conditions,
            'order' => ['Posts.created DESC'],
            'limit' => PAGE_LIMIT,
            'contain' => ['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts']
        ];
        $posts = $this->paginate($this->Posts);
        $rpcIds = [-1];
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                array_push($rpcIds, $post['r_p_c']['id']);
            }
        }
        $postRpcs = $this->Posts->find('all', ['conditions' => ['Posts.id IN' => $rpcIds]])
            ->contain(['Users', 'RPCs', 'Reposts', 'Likes', 'Comments', 'RPCPosts'])
            ->toArray();
        $postRpcs = Hash::combine($postRpcs, '{n}.id', '{n}');
        foreach ($posts as $post) {
            if ($post['r_p_c'] !== null) {
                $post['r_p_c'] = $postRpcs[$post['r_p_c']['id']];
            }
        }

        try {
            $this->set('posts', $posts);

            if ($this->request->is('json')) {
                try {
                    if (empty($this->request->getHeader('x-auth-token'))) {
                    $status = ['message' => 'No token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                } else {
                    $token = (new Parser())->parse($this->request->getHeader('x-auth-token')[0]);
                }
                } catch (RuntimeException | InvalidArgumentException $e) {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }

                if ($token->verify(new Sha256(), 'microblog')) {
                    $status = ['message' => 'Request successful', 'code' => '200'];
                    $this->set(compact('status'));
                    $this->set('_serialize', ['status', 'posts']);
                    return;
                } else {
                    $status = ['message' => 'Invalid token', 'code' => '401'];
                    $this->set(compact('status'));
                    $this->set('_serialize', 'status');
                    return;
                }
            }
        } catch (Exception $e) {
            $totalRecords = $this->posts->find('all', ['conditions' => $conditions])
                ->count();

            return $this->redirect(
                array_merge(
                    ['action' => 'posts', '?' => ['terms' => $terms]],
                    ['page' => ceil($totalRecords / PAGE_LIMIT)]
                )
            );
        }
    }
}