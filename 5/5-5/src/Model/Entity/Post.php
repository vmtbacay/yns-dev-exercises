<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Post extends Entity {
    protected function _getTitle($title) {
        return trim($title);
    }

    protected function _getBody($body) {
        return trim($body);
    }
}