<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CommentsTable extends Table {
    public function initialize(array $config) {
        $this->belongsTo('Users');
        $this->belongsTo('Posts');
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('body', 'Comment body cannot be empty')
            ->maxLength('body', 140, 'Comment body must be between 1 to 140 characters long (inclusive)')
            ->minLength('body', 1, 'Comment body must be between 1 to 140 characters long (inclusive)');

        return $validator;
    }
}