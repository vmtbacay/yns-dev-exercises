<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class FollowersTable extends Table {
    public function initialize(array $config) {
        $this->belongsTo('Users');
        $this->belongsTo('FollowerProfiles', ['className' => 'Users'])
            ->setForeignKey('follower_id');
    }
}