<?php
return [
    'number' => '<a href="{{url}}">|</span> {{text}} </a>',
    'current' => '|</span> {{text}} - You are here ',
    'ellipsis' => '| ... '
];